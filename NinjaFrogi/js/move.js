//--------------VER TECLAS--------------MOVIMENTO SAPO-----------------------//

function processaTeclas(event) {
    // verificar se a tecla pressionada é uma das pretendidas e invocar a função para adicionar ao array;
    if (event.key == 'ArrowUp' || event.key == 'ArrowDown' || event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == ' ') {
        addKey(event.key)
    }
    // mostra a lista de teclas pressionadas
    //console.log(teclas);


    //---------------------------------Movimento do background ao pressionar tecla de moviment e mudança de sprite do sapo------------------------------------------
    tecla_move = event.key;

    if (tecla_move === 'ArrowRight' && teclaBaixo === false && pausado === false || tecla_move === 'ArrowLeft' && teclaBaixo === false && pausado === false) {
        sapoSent = 'sapo';
        teclaBaixo = true;
        if (tecla_move === "ArrowLeft") {
            sapoSent = 'sapo_esq';
            intSapo = setInterval(mudaSapo, 40);
            //mudei isto nas setas
            clearInterval(intLayer);

        }
        if (tecla_move === "ArrowRight") {
            intSapo = setInterval(mudaSapo, 40);
            intLayer = setInterval(mudaLayer, 80);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------
}

function limpaTeclas(event) {
    // cada vez que uma tecla é libertada, ela é removida da lista
    if (event.key == 'ArrowUp' || event.key == 'ArrowDown' || event.key == 'ArrowLeft' || event.key == 'ArrowRight' || event.key == ' ') {
        removeKey(event.key)
    }
}

function addKey(key) {
    // adicionar teclas à lista de teclas pressionadas
    var exists = false;

    for (var i = 0; i < teclas.length; i++) {
        // verificar se já foi adicionada anteriormente
        if (teclas[i] == key)
            exists = true;
    }

    // apenas adicionar se ainda não tiver sido adicionada
    if (!exists) {
        teclas.push(key);
    }
}

function removeKey(key) {
    // remover a tecla que foi libertada
    for (var i = 0; i < teclas.length; i++) {
        if (teclas[i] == key)
            teclas.splice(i, 1);
    }
}


function executaTeclas() {


    for (var i = 0; i < teclas.length; i++) {
        switch (teclas[i]) {
            case 'ArrowLeft':

                notDeadYet();
                if (pausado === false) {

                    sapoSent = 'sapo_esq';

                    if (parseInt(sapo.style.left) < 0)
                        sapo.style.left = parseInt(sapo.style.left) + 'px';
                    else
                        sapo.style.left = parseInt(sapo.style.left) - velocidade + 'px';
                }
                break;


            case 'ArrowRight':

                notDeadYet()
                if (pausado === false) {

                    sapoSent = 'sapo';

                    if (parseInt(sapo.style.left) + 112 > 550)
                        sapo.style.left = parseInt(sapo.style.left) + 'px';
                    else
                        sapo.style.left = parseInt(sapo.style.left) + velocidade + 'px';
                }
                break;


            case ' ':

                notDeadYet()
                if (pausado === false) {
                    if (aSaltar === false && (typeof(saltitaTimer)) === 'undefined' && (typeof(saltinhos)) === 'undefined') {
                        somSaltar.play();
                        if (parseInt(sapo.style.left) + 112 > 400) {
                            saltaLayer = setInterval(mudaLayer, 80);
                            saltitaTimer = setInterval(salta, 20);
                            saltinhos = setInterval(mudaSapo, 18000);
                        } else {
                            saltitaTimer = setInterval(salta, 20);
                            saltinhos = setInterval(mudaSapo, 18000);
                        }
                    }
                }
                break;

            case "m":
                if (somMute === false) {
                    somMute = true;
                }
                else
                    somMute = false;
                break;


        }
    }
}

function moveSeta(sentido) {
    var tecla = sentido.key;
    if (auxilioBatalha === true) {
        clearInterval(saltaLayer);
        switch (tecla) {
            case 'ArrowDown':

                //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
                //------------------Variaveis do som--------------------//
                somMudarSeta.pause();
                if (somMudarSeta.paused)
                    somMudarSeta.play();
                //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
                //------------------Variaveis do som-------------------//

                if (parseInt(setaOpcao.style.top) >= 570)
                    setaOpcao.style.top = parseInt(setaOpcao.style.top) + 'px';
                else
                    setaOpcao.style.top = parseInt(setaOpcao.style.top) + 30 + 'px';
                break;
            case 'ArrowUp':
                //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
                //------------------Variaveis do som---------------------//
                somMudarSeta.pause();
                if (somMudarSeta.paused)
                    somMudarSeta.play();
                //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
                //------------------Variaveis do som---------------------//
                if (parseInt(setaOpcao.style.top) <= 540)
                    setaOpcao.style.top = parseInt(setaOpcao.style.top) + 'px';
                else
                    setaOpcao.style.top = parseInt(setaOpcao.style.top) - 30 + 'px';
                break;
            //
            case 'Enter':
                chanceFugir = Math.random();
                if (estaMenuBatalha === false) {
                    if (auxilioNumCliquesBatalha === 0) {
                        if (parseInt(setaOpcao.style.top) >= 570) {
                            if (vidaAtual > (maxVida / 2)) {
                                if (chanceFugir <= 0.2) {
                                    sairModoBatalha();
                                    pontuacao -= 20;
                                }
                                else {
                                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                                    if (nlvl === 1 || nlvl === 2 || nlvl === 3 || nlvl === 4) {
                                        auxilioNumCliquesBatalha++;
                                        if (estaMenuBatalha === false) {
                                            if (auxilioNumCliquesBatalha === 1) {
                                                estaMenuBatalha = false;
                                                modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><td>Tongue Attack</td></tr><tr><td>Frog Splash</td></tr></table>';
                                            }
                                            else
                                                posicaoSeta();
                                        }
                                    }

                                    if (nlvl === 5 || nlvl === 6 || nlvl === 7 || nlvl === 8) {
                                        auxilioNumCliquesBatalha++;
                                        if (estaMenuBatalha === false) {
                                            if (auxilioNumCliquesBatalha === 1) {
                                                estaMenuBatalha = false;
                                                modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><th>Tongue Attack</th></tr><tr><th>Gastro Shuriken</th></tr></table>';
                                            }
                                            else
                                                posicaoSeta();
                                        }
                                    }
                                    if (nlvl === 9 || nlvl === 10) {
                                        auxilioNumCliquesBatalha++;
                                        if (estaMenuBatalha === false) {
                                            if (auxilioNumCliquesBatalha === 1) {
                                                estaMenuBatalha = false;
                                                modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><th>Tongue Attack</th></tr><tr><th>Ninja Shield</th></tr></table>';
                                            }
                                            else
                                                posicaoSeta();
                                        }
                                    }
                                    //
                                }
                            }
                            if (vidaAtual <= (maxVida / 2)) {
                                if (chanceFugir <= 0.1) {
                                    sairModoBatalha();
                                    pontuacao -= 20;
                                }
                                else {
                                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                                    if (nlvl === 1 || nlvl === 2 || nlvl === 3 || nlvl === 4) {
                                        auxilioNumCliquesBatalha++;
                                        if (estaMenuBatalha === false) {
                                            if (auxilioNumCliquesBatalha === 1) {
                                                estaMenuBatalha = false;
                                                modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><td>Tongue Attack</td></tr><tr><td>Frog Splash</td></tr></table>';
                                            }
                                            else
                                                posicaoSeta();
                                        }
                                    }

                                    if (nlvl === 5 || nlvl === 6 || nlvl === 7 || nlvl === 8) {
                                        auxilioNumCliquesBatalha++;
                                        if (estaMenuBatalha === false) {
                                            if (auxilioNumCliquesBatalha === 1) {
                                                estaMenuBatalha = false;
                                                modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><th>Tongue Attack</th></tr><tr><th>Gastro Shuriken</th></tr></table>';
                                            }
                                            else
                                                posicaoSeta();
                                        }
                                    }
                                    if (nlvl === 9 || nlvl === 10) {
                                        auxilioNumCliquesBatalha++;
                                        if (estaMenuBatalha === false) {
                                            if (auxilioNumCliquesBatalha === 1) {
                                                estaMenuBatalha = false;
                                                modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><th>Tongue Attack</th></tr><tr><th>Ninja Shield</th></tr></table>';
                                            }
                                            else
                                                posicaoSeta();
                                        }
                                    }
                                    //
                                }
                            }
                        }
                        else {
                            if (nlvl === 1 || nlvl === 2 || nlvl === 3 || nlvl === 4) {
                                auxilioNumCliquesBatalha++;
                                if (estaMenuBatalha === false) {
                                    if (auxilioNumCliquesBatalha === 1) {
                                        estaMenuBatalha = false;
                                        modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><td>Tongue Attack</td></tr><tr><td>Frog Splash</td></tr></table>';
                                    }
                                    else
                                        posicaoSeta();
                                }
                            }

                            if (nlvl === 5 || nlvl === 6 || nlvl === 7 || nlvl === 8) {
                                auxilioNumCliquesBatalha++;
                                if (estaMenuBatalha === false) {
                                    if (auxilioNumCliquesBatalha === 1) {
                                        estaMenuBatalha = false;
                                        modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><th>Tongue Attack</th></tr><tr><th>Gastro Shuriken</th></tr></table>';
                                    }
                                    else
                                        posicaoSeta();
                                }
                            }
                            if (nlvl === 9 || nlvl === 10) {
                                auxilioNumCliquesBatalha++;
                                if (estaMenuBatalha === false) {
                                    if (auxilioNumCliquesBatalha === 1) {
                                        estaMenuBatalha = false;
                                        modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><th>Tongue Attack</th></tr><tr><th>Ninja Shield</th></tr></table>';
                                    }
                                    else
                                        posicaoSeta();
                                }
                            }
                            //
                        }
                    }
                    else {
                        if (nlvl === 1 || nlvl === 2 || nlvl === 3 || nlvl === 4) {
                            auxilioNumCliquesBatalha++;
                            if (estaMenuBatalha === false) {
                                if (auxilioNumCliquesBatalha === 1) {
                                    estaMenuBatalha = false;
                                    modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><td>Tongue Attack</td></tr><tr><td>Frog Splash</td></tr></table>';
                                }
                                else
                                    posicaoSeta();
                            }
                        }

                        if (nlvl === 5 || nlvl === 6 || nlvl === 7 || nlvl === 8) {
                            auxilioNumCliquesBatalha++;
                            if (estaMenuBatalha === false) {
                                if (auxilioNumCliquesBatalha === 1) {
                                    estaMenuBatalha = false;
                                    modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><th>Tongue Attack</th></tr><tr><th>Gastro Shuriken</th></tr></table>';
                                }
                                else
                                    posicaoSeta();
                            }
                        }
                        if (nlvl === 9 || nlvl === 10) {
                            auxilioNumCliquesBatalha++;
                            if (estaMenuBatalha === false) {
                                if (auxilioNumCliquesBatalha === 1) {
                                    estaMenuBatalha = false;
                                    modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><th>Tongue Attack</th></tr><tr><th>Ninja Shield</th></tr></table>';
                                }
                                else
                                    posicaoSeta();
                            }
                        }
                        //
                    }
                }
        }
    }

    function turnoInimigo() {
        chanceFalharAtaque = Math.random();
        if (chanceFalharAtaque <= 0.1) {
            feedback.style.top = '200px';
            feedback.style.left = '10px';
            feedback.innerHTML = '<h1>MISS</h1>';
        }
        else {
            if (auxilioQualAtaqueInimigo === 0) {
                vidaAtual -= ataqueInimigo1;

                feedback.style.top = '200px';
                feedback.style.left = '10px';
                feedback.innerHTML = '<h1>Perdeste ' + ataqueInimigo1 + 'HP</h1>';
                //
                quantidadeDeVida.style.width = vidaAtual + "px";
            }
            else {
                vidaAtual -= ataqueInimigo2;

                feedback.style.top = '200px';
                feedback.style.left = '10px';
                feedback.innerHTML = '<h1>Perdeste ' + ataqueInimigo2 + 'HP</h1>';
                //
                quantidadeDeVida.style.width = vidaAtual + "px";
            }
        }
        estaMenuBatalha = false;
    }
}

function posicaoSeta() {
    estaMenuBatalha = true;
    if (nlvl === 1 || nlvl === 2 || nlvl === 3 || nlvl === 4 || nlvl === 5 || nlvl === 6 || nlvl === 7 || nlvl === 8 || nlvl === 9 || nlvl === 10) {
        if (parseInt(setaOpcao.style.top) <= 540) {
            //console.log('ola atk1');
            retiraVida('atk1');
        } else {
            retiraVida('atk2');
            //console.log('ola atk2');
        }
    }

}

function retiraVida(numAtaque) {
    chanceFalharAtaque = Math.random();
    if (nlvl === 1 || nlvl === 2 || nlvl === 3 || nlvl === 4) {
        if (vidaInimigo <= 0) {
            sairModoBatalha();
        } else if (numAtaque === 'atk1') {
            if (chanceFalharAtaque > 0.1) {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeed1, 1000);

                function esperafeed1() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    vidaInimigo -= 20;

                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>Tirou 20HP</h1>';
                    //
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    //console.log('vidaInimiga: ' + vidaInimigo);
                    //console.log('vida sapo: ' + vidaAtual);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
            else {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeedA, 1000);

                function esperafeedA() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>MISS</h1>';
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
        }
        else {
            if (chanceFalharAtaque > 0.2) {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeed2, 1000);

                function esperafeed2() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    vidaInimigo -= 30;

                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>Tirou 30HP</h1>';
                    //
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
            else {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeedB, 1000);

                function esperafeedB() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>MISS</h1>';
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
        }
    }

    if (nlvl === 5 || nlvl === 6 || nlvl === 7 || nlvl === 8) {
        if (vidaInimigo <= 0) {
            sairModoBatalha();
        } else if (numAtaque === 'atk1') {
            if (chanceFalharAtaque > 0.1) {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeed3, 1000);

                function esperafeed3() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    vidaInimigo -= 40;

                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>Tirou 40HP</h1>';
                    //
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    //console.log('vidaInimiga: ' + vidaInimigo);
                    //console.log('vida sapo: ' + vidaAtual);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
            else {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeedC, 1000);

                function esperafeedC() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>MISS</h1>';
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
        }
        else {
            if (chanceFalharAtaque > 0.2) {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeed4, 1000);

                function esperafeed4() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    vidaInimigo -= 85;

                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>Tirou 85HP</h1>';
                    //
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
            else {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeedD, 1000);

                function esperafeedD() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>MISS</h1>';
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
        }
    }
    if (nlvl === 9 || nlvl === 10) {
        if (vidaInimigo <= 0) {
            sairModoBatalha();
        } else if (numAtaque === 'atk1') {
            if (chanceFalharAtaque > 0.1) {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeed5, 1000);

                function esperafeed5() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    vidaInimigo -= 60;

                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>Tirou 60HP</h1>';
                    //
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    //console.log('vidaInimiga: ' + vidaInimigo);
                    //console.log('vida sapo: ' + vidaAtual);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
            else {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeedE, 1000);

                function esperafeedE() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>MISS</h1>';
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
        }
        else {
            if (chanceFalharAtaque > 0.2) {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeed6, 1000);

                function esperafeed6() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    vidaInimigo -= 125;

                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>Tirou 125HP</h1>';
                    //
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
            else {
                modoBatalhaSapoAnimacao = setInterval(mudaSapo, 40);
                deslocaSapoBatalhaAnimacao = setInterval(deslocaSapoEmBatalha, 20);
                esperaAnimacaoBatalha = setTimeout(esperafeedF, 1000);

                function esperafeedF() {
                    clearInterval(modoBatalhaSapoAnimacao);
                    feedback.style.top = '200px';
                    feedback.style.left = '550px';
                    feedback.innerHTML = '<h1>MISS</h1>';
                    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);
                    deslocaBatalhaInimigoAnimacao = setInterval(deslocaInimigo, 20);
                    turnoDoIninimigo = setTimeout(turnoInimigo, 1000);
                }
            }
        }
    }
    //
    auxilioQualAtaqueInimigo = parseInt(Math.random() * 2);

    function turnoInimigo() {
        chanceFalharAtaque = Math.random();
        if (chanceFalharAtaque <= 0.1) {
            feedback.style.top = '200px';
            feedback.style.left = '10px';
            feedback.innerHTML = '<h1>MISS</h1>';
        }
        else {
            if (auxilioQualAtaqueInimigo === 0) {
                vidaAtual -= ataqueInimigo1;

                feedback.style.top = '200px';
                feedback.style.left = '10px';
                feedback.innerHTML = '<h1>Perdeste ' + ataqueInimigo1 + 'HP</h1>';
                //
                quantidadeDeVida.style.width = vidaAtual + "px";
            }
            else {
                vidaAtual -= ataqueInimigo2;

                feedback.style.top = '200px';
                feedback.style.left = '10px';
                feedback.innerHTML = '<h1>Perdeste ' + ataqueInimigo2 + 'HP</h1>';
                //
                quantidadeDeVida.style.width = vidaAtual + "px";
            }
        }
        estaMenuBatalha = false;
        if (vidaAtual <= 0) {
            contVidas--;
            if (contVidas === 2) {
                document.getElementById('vida3Img').style.visibility = 'hidden';
            }
            if (contVidas === 1) {
                document.getElementById('vida2Img').style.visibility = 'hidden';
            }
            if (contVidas === 0) {
                document.getElementById('vida1Img').style.visibility = 'hidden';
                fimJogo();
            }
            sairModoBatalha();
            pontuacao -= 20;
            divpontuacao.innerHTML = '<h1>' + pontuacao + '</h1>';
            if (nlvl === 1 || nlvl === 2 || nlvl === 3 || nlvl === 4) {
                vidaAtual = 150;
                quantidadeDeVida.style.width = vidaAtual + "px";
            }
            if (nlvl === 5 || nlvl === 6 || nlvl === 7 || nlvl === 8) {
                vidaAtual = 350;
                quantidadeDeVida.style.width = vidaAtual + "px";
            }
            if (nlvl === 9 || nlvl === 10) {
                vidaAtual = 750;
                quantidadeDeVida.style.width = vidaAtual + "px";
            }
        }
    }
}

function sairModoBatalha() {

//x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
//------------------Variaveis do som---------------------//
    somFundo1.play();
    somMudarSeta.pause();
    somSaltar.pause();
    somModoBatalha.pause();
    somGanhaVidaOuXP.pause();
    somPerdeVidaOuHP.pause();


//x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//

    pausado = false;
    auxilioBatalha = false;
    estaMenuBatalha = false;
    modoBatalha.style.visibility = 'hidden';
    setaOpcao.style.visibility = 'hidden';
    pontuacao += 20;
    divpontuacao.innerHTML = '<h1>' + pontuacao + '</h1>';
    sapo.style.left = parseInt(auxilioPosicaoSapo) + 'px';

    // bg1 e 2
    desSardao = setInterval(mudaSardao, 200);
    moveSardao = setInterval(deslocaSardao, 40);

    desMocho = setInterval(mudaMocho, 200);
    moveMocho = setInterval(deslocaMocho, 10);

    desOurico = setInterval(mudaOurico, 200);
    moveOurico = setInterval(deslocaOurico, 40);

    desNinjaVerde = setInterval(mudaNinjaVerde, 200);

    desNinjaAzul = setInterval(mudaNinjaAzul, 200);
    // bg3
    desCobra = setInterval(mudaCobra, 200);
    moveCobra = setInterval(deslocaCobra, 40);

    desSardao2 = setInterval(mudaSardao2, 200);
    moveSardao2 = setInterval(deslocaSardao2, 40);

    desBat = setInterval(mudaBat, 200);
    moveBat = setInterval(deslocaBat, 40);

    desNinjaRoxo = setInterval(mudaNinjaRoxo, 200);
    // bg4
    desDodo = setInterval(mudaDodo, 200);
    moveDodo = setInterval(deslocaDodo, 40);

    desSardao3 = setInterval(mudaSardao3, 200);
    moveSardao3 = setInterval(deslocaSardao3, 40);

    desPassaro = setInterval(mudaPassaro, 200);
    movePassaro = setInterval(deslocaPassaro, 40);

    desNinjaVermelho = setInterval(mudaNinjaVermelho, 200);
    // bg5
    desCrocodilo = setInterval(mudaCrocodilo, 200);
    moveCrocodilo = setInterval(deslocaCrocodilo, 40);

    desRaccoon = setInterval(mudaRaccoon, 200);
    moveRaccoon = setInterval(deslocaRaccoon, 40);

    desAguia = setInterval(mudaAguia, 200);
    moveAguia = setInterval(deslocaAguia, 40);

    desNinjaPreto = setInterval(mudaNinjaPreto, 200);
    //
    auxilioNumCliquesBatalha = 0;
    modoBatalha.innerHTML = '<table style="vertical-align: middle"><tr><th>ATACAR</th></tr><tr><th>FUGIR</th></tr></table>';
    feedback.innerHTML = ' ';
    // sardao 1
    if (auxilioNomeInimigo === 'sardao1') {
        document.getElementById('sardao1').style.visibility = 'hidden';
        document.getElementById('sardao1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'sardao2') {
        document.getElementById('sardao2').alt = 'morreu';
        document.getElementById('sardao2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'sardao3') {
        document.getElementById('sardao3').alt = 'morreu';
        document.getElementById('sardao3').style.visibility = 'hidden';
    }

    // sardao 2
    if (auxilioNomeInimigo === 'sardao2_1') {
        document.getElementById('sardao2_1').style.visibility = 'hidden';
        document.getElementById('sardao2_1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'sardao2_2') {
        document.getElementById('sardao2_2').alt = 'morreu';
        document.getElementById('sardao2_2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'sardao2_3') {
        document.getElementById('sardao2_3').alt = 'morreu';
        document.getElementById('sardao2_3').style.visibility = 'hidden';
    }

    // sardao 2
    if (auxilioNomeInimigo === 'sardao3_1') {
        document.getElementById('sardao3_1').style.visibility = 'hidden';
        document.getElementById('sardao3_1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'sardao3_2') {
        document.getElementById('sardao3_2').alt = 'morreu';
        document.getElementById('sardao3_2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'sardao3_3') {
        document.getElementById('sardao3_3').alt = 'morreu';
        document.getElementById('sardao3_3').style.visibility = 'hidden';
    }

    // mocho

    if (auxilioNomeInimigo === 'mocho1') {
        document.getElementById('mocho1').style.visibility = 'hidden';
        document.getElementById('mocho1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'mocho2') {
        document.getElementById('mocho2').alt = 'morreu';
        document.getElementById('mocho2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'mocho3') {
        document.getElementById('mocho3').alt = 'morreu';
        document.getElementById('mocho3').style.visibility = 'hidden';
    }

    // ouriço

    if (auxilioNomeInimigo === 'ourico1') {
        document.getElementById('ourico1').style.visibility = 'hidden';
        document.getElementById('ourico1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'ourico2') {
        document.getElementById('ourico2').alt = 'morreu';
        document.getElementById('ourico2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'ourico3') {
        document.getElementById('ourico3').alt = 'morreu';
        document.getElementById('ourico3').style.visibility = 'hidden';
    }

    // cobra

    if (auxilioNomeInimigo === 'cobra1') {
        document.getElementById('cobra1').style.visibility = 'hidden';
        document.getElementById('cobra1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'cobra2') {
        document.getElementById('cobra2').alt = 'morreu';
        document.getElementById('cobra2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'cobra3') {
        document.getElementById('cobra3').alt = 'morreu';
        document.getElementById('cobra3').style.visibility = 'hidden';
    }

    // bat

    if (auxilioNomeInimigo === 'bat1') {
        document.getElementById('bat1').style.visibility = 'hidden';
        document.getElementById('bat1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'bat2') {
        document.getElementById('bat2').alt = 'morreu';
        document.getElementById('bat2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'bat3') {
        document.getElementById('bat3').alt = 'morreu';
        document.getElementById('bat3').style.visibility = 'hidden';
    }

    // racoon

    if (auxilioNomeInimigo === 'raccoon1') {
        document.getElementById('raccoon1').style.visibility = 'hidden';
        document.getElementById('raccoon1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'raccoon2') {
        document.getElementById('raccoon2').alt = 'morreu';
        document.getElementById('raccoon2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'raccoon3') {
        document.getElementById('raccoon3').alt = 'morreu';
        document.getElementById('raccoon3').style.visibility = 'hidden';
    }

    // aguia

    if (auxilioNomeInimigo === 'aguia1') {
        document.getElementById('aguia1').style.visibility = 'hidden';
        document.getElementById('aguia1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'aguia2') {
        document.getElementById('aguia2').alt = 'morreu';
        document.getElementById('aguia2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'aguia3') {
        document.getElementById('aguia3').alt = 'morreu';
        document.getElementById('aguia3').style.visibility = 'hidden';
    }

    // passaro

    if (auxilioNomeInimigo === 'passaro1') {
        document.getElementById('passaro1').style.visibility = 'hidden';
        document.getElementById('passaro1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'passaro2') {
        document.getElementById('passaro2').alt = 'morreu';
        document.getElementById('passaro2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'passaro3') {
        document.getElementById('passaro3').alt = 'morreu';
        document.getElementById('passaro3').style.visibility = 'hidden';
    }

    // dodo

    if (auxilioNomeInimigo === 'dodo1') {
        document.getElementById('dodo1').style.visibility = 'hidden';
        document.getElementById('dodo1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'dodo2') {
        document.getElementById('dodo2').alt = 'morreu';
        document.getElementById('dodo2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'dodo3') {
        document.getElementById('dodo3').alt = 'morreu';
        document.getElementById('dodo3').style.visibility = 'hidden';
    }

    // crocodilo

    if (auxilioNomeInimigo === 'crocodilo1') {
        document.getElementById('crocodilo1').style.visibility = 'hidden';
        document.getElementById('crocodilo1').alt = 'morreu';
    }
    if (auxilioNomeInimigo === 'crocodilo2') {
        document.getElementById('crocodilo2').alt = 'morreu';
        document.getElementById('crocodilo2').style.visibility = 'hidden';
    }
    if (auxilioNomeInimigo === 'crocodilo3') {
        document.getElementById('crocodilo3').alt = 'morreu';
        document.getElementById('crocodilo3').style.visibility = 'hidden';
    }
    // NINJAS

    if (auxilioNomeInimigo === 'ninja_verde') {
        document.getElementById('ninja_verde').style.visibility = 'hidden';
        document.getElementById('ninja_verde').alt = 'morreu';
        feedback.innerHTML = ' ';
        fazResetParaLvlUp();
    }
    if (auxilioNomeInimigo === 'ninja_azul') {
        document.getElementById('ninja_azul').style.visibility = 'hidden';
        document.getElementById('ninja_azul').alt = 'morreu';
        feedback.innerHTML = ' ';
        fazResetParaLvlUp();
    }
    if (auxilioNomeInimigo === 'ninja_vermelho') {
        document.getElementById('ninja_vermelho').style.visibility = 'hidden';
        document.getElementById('ninja_vermelho').alt = 'morreu';
        feedback.innerHTML = ' ';
        fazResetParaLvlUp();
    }
    if (auxilioNomeInimigo === 'ninja_roxo') {
        document.getElementById('ninja_roxo').style.visibility = 'hidden';
        document.getElementById('ninja_roxo').alt = 'morreu';
        feedback.innerHTML = ' ';
        fazResetParaLvlUp();
    }
    if (auxilioNomeInimigo === 'ninja_preto') {
        document.getElementById('ninja_preto').style.visibility = 'hidden';
        document.getElementById('ninja_preto').alt = 'morreu';
        feedback.innerHTML = ' ';
        fazResetParaLvlUp();
    }
}

//-------------------Nova funcao--------------------------//
function salta() {
    aSaltar = true;
    sapo.style.top = parseInt(sapo.style.top) - velocidadeVertical + 'px';
    if (parseInt(sapo.style.top) < 140)
        velocidadeVertical = -velocidadeVertical;
    if (parseInt(sapo.style.top) > (600 - 90 - 106)) {
        if (parseInt(sapo.style.left) + 112 > 400) {
            clearInterval(saltaLayer);
        }
        velocidadeVertical = -velocidadeVertical;
        clearInterval(saltitaTimer);
        delete saltitaTimer;
        clearInterval(saltinhos);
        delete saltinhos;
        aSaltar = false;

    }

}

function carregaJogo() {
    pausado = false;
    //-----Aqui esta o incremento do nivel e o numero a mudar de nivel bem como o update ao mudaSapo e inimigos para baixo- --------//
    if (varDeOpcao !== true) {

        nlvl++;
        if (nlvl === 1 || nlvl === 2 || nlvl === 3 || nlvl === 4) {
            maxVida = 150;
            vidaAtual = 150;
            quantidadeMaxVida.style.width = maxVida + 'px';
            quantidadeDeVida.style.width = vidaAtual + "px";
            if (nlvl === 2) {
                document.getElementById('ninja_verde').style.visibility = 'visible';
                desNinjaVerde = setInterval(mudaNinjaVerde, 200);

                for (var n7 = 1; n7 <= 3; n7++) {
                    var nomeSardaonlvl7 = 'sardao' + n7;
                    var nomeMochonlvl7 = 'mocho' + n7;
                    var nomeOuriconlvl7 = 'ourico' + n7;

                    var nomeSardao2nlvl7 = 'sardao2_' + n7;
                    var nomeCobralvl7 = 'cobra' + n7;
                    var nomeBatnlvl7 = 'bat' + n7;

                    var nomeSardao3nlvl7 = 'sardao3_' + n7;
                    var nomeDodolvl7 = 'dodo' + n7;
                    var nomePassaronlvl7 = 'passaro' + n7;

                    var nomeAguialvl7 = 'aguia' + n7;
                    var nomeCrocodilolvl7 = 'crocodilo' + n7;
                    var nomeRaccoonlvl7 = 'raccoon' + n7;

                    document.getElementById(nomeSardaonlvl7).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl7).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl7).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl7).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl7).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl7).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl7).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl7).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl7).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl7).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl7).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl7).style.visibility = 'hidden';

                    document.getElementById(nomeSardaonlvl7).alt = 'morreu';
                    document.getElementById(nomeMochonlvl7).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl7).alt = 'morreu';

                    document.getElementById(nomeCobralvl7).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl7).alt = 'morreu';
                    document.getElementById(nomeBatnlvl7).alt = 'morreu';

                    document.getElementById(nomeDodolvl7).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl7).alt = 'morreu';
                    document.getElementById(nomePassaronlvl7).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl7).alt = 'morreu';
                    document.getElementById(nomeAguialvl7).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl7).alt = 'morreu';
                }
                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';
            }
            else if (nlvl === 1 || nlvl === 3) {
                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                for (var n1 = 1; n1 <= 3; n1++) {
                    var nomeSardaonlvl1 = 'sardao' + n1;
                    var nomeMochonlvl1 = 'mocho' + n1;
                    var nomeOuriconlvl1 = 'ourico' + n1;

                    var nomeSardao2nlvl1 = 'sardao2_' + n1;
                    var nomeCobralvl1 = 'cobra' + n1;
                    var nomeBatnlvl1 = 'bat' + n1;

                    var nomeSardao3nlvl1 = 'sardao3_' + n1;
                    var nomeDodolvl1 = 'dodo' + n1;
                    var nomePassaronlvl1 = 'passaro' + n1;

                    var nomeAguialvl1 = 'aguia' + n1;
                    var nomeCrocodilolvl1 = 'crocodilo' + n1;
                    var nomeRaccoonlvl1 = 'raccoon' + n1;

                    document.getElementById(nomeSardaonlvl1).style.visibility = 'visible';
                    document.getElementById(nomeMochonlvl1).style.visibility = 'visible';
                    document.getElementById(nomeOuriconlvl1).style.visibility = 'visible';

                    document.getElementById(nomeSardao2nlvl1).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl1).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl1).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl1).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl1).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl1).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl1).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl1).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl1).style.visibility = 'hidden';

                    document.getElementById(nomeCobralvl1).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl1).alt = 'morreu';
                    document.getElementById(nomeBatnlvl1).alt = 'morreu';

                    document.getElementById(nomeDodolvl1).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl1).alt = 'morreu';
                    document.getElementById(nomePassaronlvl1).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl1).alt = 'morreu';
                    document.getElementById(nomeAguialvl1).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl1).alt = 'morreu';
                }
                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                desSardao = setInterval(mudaSardao, 200);
                moveSardao = setInterval(deslocaSardao, 40);
                desMocho = setInterval(mudaMocho, 200);
                moveMocho = setInterval(deslocaMocho, 10);
                desOurico = setInterval(mudaOurico, 200);
                moveOurico = setInterval(deslocaOurico, 40);
            }
            if (nlvl === 4) {
                document.getElementById('ninja_azul').style.visibility = 'visible';
                desNinjaAzul = setInterval(mudaNinjaAzul, 200);

                for (var n9 = 1; n9 <= 3; n9++) {
                    var nomeSardaonlvl9 = 'sardao' + n9;
                    var nomeMochonlvl9 = 'mocho' + n9;
                    var nomeOuriconlvl9 = 'ourico' + n9;

                    var nomeSardao2nlvl9 = 'sardao2_' + n9;
                    var nomeCobralvl9 = 'cobra' + n9;
                    var nomeBatnlvl9 = 'bat' + n9;

                    var nomeSardao3nlvl9 = 'sardao3_' + n9;
                    var nomeDodolvl9 = 'dodo' + n9;
                    var nomePassaronlvl9 = 'passaro' + n9;

                    var nomeAguialvl9 = 'aguia' + n9;
                    var nomeCrocodilolvl9 = 'crocodilo' + n9;
                    var nomeRaccoonlvl9 = 'raccoon' + n9;

                    document.getElementById(nomeSardaonlvl9).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl9).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl9).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl9).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl9).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl9).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl9).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl9).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl9).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl9).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl9).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl9).style.visibility = 'hidden';

                    document.getElementById(nomeSardaonlvl9).alt = 'morreu';
                    document.getElementById(nomeMochonlvl9).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl9).alt = 'morreu';

                    document.getElementById(nomeCobralvl9).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl9).alt = 'morreu';
                    document.getElementById(nomeBatnlvl9).alt = 'morreu';

                    document.getElementById(nomeDodolvl9).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl9).alt = 'morreu';
                    document.getElementById(nomePassaronlvl9).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl9).alt = 'morreu';
                    document.getElementById(nomeAguialvl9).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl9).alt = 'morreu';
                }
                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';
            }
            else {
                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';
            }
        }
        if (nlvl === 5 || nlvl === 6) {
            maxVida = 350;
            vidaAtual = 350;
            quantidadeMaxVida.style.width = maxVida + 'px';
            quantidadeDeVida.style.width = vidaAtual + "px";
            if (nlvl === 6) {
                document.getElementById('ninja_roxo').style.visibility = 'visible';
                desNinjaRoxo = setInterval(mudaNinjaRoxo, 200);

                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';

                for (var n6 = 1; n6 <= 3; n6++) {

                    var nomeSardaonlvl6 = 'sardao' + n6;
                    var nomeMochonlvl6 = 'mocho' + n6;
                    var nomeOuriconlvl6 = 'ourico' + n6;

                    var nomeSardao2nlvl6 = 'sardao2_' + n6;
                    var nomeCobralvl6 = 'cobra' + n6;
                    var nomeBatnlvl6 = 'bat' + n6;

                    var nomeSardao3nlvl6 = 'sardao3_' + n6;
                    var nomeDodolvl6 = 'dodo' + n6;
                    var nomePassaronlvl6 = 'passaro' + n6;

                    var nomeAguialvl6 = 'aguia' + n6;
                    var nomeCrocodilolvl6 = 'crocodilo' + n6;
                    var nomeRaccoonlvl6 = 'raccoon' + n6;

                    document.getElementById(nomeSardaonlvl6).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl6).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl6).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl6).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl6).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl6).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl6).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl6).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl6).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl6).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl6).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl6).style.visibility = 'hidden';

                    document.getElementById(nomeSardaonlvl6).alt = 'morreu';
                    document.getElementById(nomeMochonlvl6).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl6).alt = 'morreu';

                    document.getElementById(nomeSardao2nlvl6).alt = 'morreu';
                    document.getElementById(nomeCobralvl6).alt = 'morreu';
                    document.getElementById(nomeBatnlvl6).alt = 'morreu';

                    document.getElementById(nomeDodolvl6).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl6).alt = 'morreu';
                    document.getElementById(nomePassaronlvl6).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl6).alt = 'morreu';
                    document.getElementById(nomeAguialvl6).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl6).alt = 'morreu';

                }
            }
            else {
                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                for (var n2 = 1; n2 <= 3; n2++) {

                    var nomeSardaonlvl2 = 'sardao' + n2;
                    var nomeMochonlvl2 = 'mocho' + n2;
                    var nomeOuriconlvl2 = 'ourico' + n2;

                    var nomeSardao2nlvl2 = 'sardao2_' + n2;
                    var nomeCobralvl2 = 'cobra' + n2;
                    var nomeBatnlvl2 = 'bat' + n2;

                    var nomeSardao3nlvl2 = 'sardao3_' + n2;
                    var nomeDodolvl2 = 'dodo' + n2;
                    var nomePassaronlvl2 = 'passaro' + n2;

                    var nomeAguialvl2 = 'aguia' + n2;
                    var nomeCrocodilolvl2 = 'crocodilo' + n2;
                    var nomeRaccoonlvl2 = 'raccoon' + n2;

                    document.getElementById(nomeSardaonlvl2).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl2).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl2).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl2).style.visibility = 'visible';
                    document.getElementById(nomeCobralvl2).style.visibility = 'visible';
                    document.getElementById(nomeBatnlvl2).style.visibility = 'visible';

                    document.getElementById(nomeDodolvl2).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl2).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl2).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl2).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl2).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl2).style.visibility = 'hidden';

                    document.getElementById(nomeSardaonlvl2).alt = 'morreu';
                    document.getElementById(nomeMochonlvl2).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl2).alt = 'morreu';

                    document.getElementById(nomeDodolvl2).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl2).alt = 'morreu';
                    document.getElementById(nomePassaronlvl2).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl2).alt = 'morreu';
                    document.getElementById(nomeAguialvl2).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl2).alt = 'morreu';

                }
                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';

                desCobra = setInterval(mudaCobra, 200);
                moveCobra = setInterval(deslocaCobra, 40);
                desBat = setInterval(mudaBat, 200);
                moveBat = setInterval(deslocaBat, 40);
                desSardao2 = setInterval(mudaSardao2, 200);
                moveSardao2 = setInterval(deslocaSardao2, 40);
            }
        }
        if (nlvl === 7 || nlvl === 8) {
            maxVida = 350;
            vidaAtual = 350;
            quantidadeMaxVida.style.width = maxVida + 'px';
            quantidadeDeVida.style.width = vidaAtual + "px";
            if (nlvl === 8) {
                document.getElementById('ninja_vermelho').style.visibility = 'visible';
                desNinjaVermelho = setInterval(mudaNinjaVermelho, 200);

                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';

                for (var n8 = 1; n8 <= 3; n8++) {
                    var nomeSardaonlvl8 = 'sardao' + n8;
                    var nomeMochonlvl8 = 'mocho' + n8;
                    var nomeOuriconlvl8 = 'ourico' + n8;

                    var nomeSardao2nlvl8 = 'sardao2_' + n8;
                    var nomeCobralvl8 = 'cobra' + n8;
                    var nomeBatnlvl8 = 'bat' + n8;

                    var nomeSardao3nlvl8 = 'sardao3_' + n8;
                    var nomeDodolvl8 = 'dodo' + n8;
                    var nomePassaronlvl8 = 'passaro' + n8;

                    var nomeAguialvl8 = 'aguia' + n8;
                    var nomeCrocodilolvl8 = 'crocodilo' + n8;
                    var nomeRaccoonlvl8 = 'raccoon' + n8;

                    document.getElementById(nomeSardaonlvl8).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl8).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl8).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl8).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl8).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl8).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl8).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl8).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl8).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl8).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl8).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl8).style.visibility = 'hidden';

                    document.getElementById(nomeCobralvl8).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl8).alt = 'morreu';
                    document.getElementById(nomeBatnlvl8).alt = 'morreu';

                    document.getElementById(nomeSardaonlvl8).alt = 'morreu';
                    document.getElementById(nomeMochonlvl8).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl8).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl8).alt = 'morreu';
                    document.getElementById(nomeAguialvl8).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl8).alt = 'morreu';

                    document.getElementById(nomeDodolvl8).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl8).alt = 'morreu';
                    document.getElementById(nomePassaronlvl8).alt = 'morreu';
                }
                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';
            }
            else {
                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                for (var n3 = 1; n3 <= 3; n3++) {
                    var nomeSardaonlvl3 = 'sardao' + n3;
                    var nomeMochonlvl3 = 'mocho' + n3;
                    var nomeOuriconlvl3 = 'ourico' + n3;

                    var nomeSardao2nlvl3 = 'sardao2_' + n3;
                    var nomeCobralvl3 = 'cobra' + n3;
                    var nomeBatnlvl3 = 'bat' + n3;

                    var nomeSardao3nlvl3 = 'sardao3_' + n3;
                    var nomeDodolvl3 = 'dodo' + n3;
                    var nomePassaronlvl3 = 'passaro' + n3;

                    var nomeAguialvl3 = 'aguia' + n3;
                    var nomeCrocodilolvl3 = 'crocodilo' + n3;
                    var nomeRaccoonlvl3 = 'raccoon' + n3;

                    document.getElementById(nomeSardaonlvl3).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl3).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl3).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl3).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl3).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl3).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl3).style.visibility = 'visible';
                    document.getElementById(nomeSardao3nlvl3).style.visibility = 'visible';
                    document.getElementById(nomePassaronlvl3).style.visibility = 'visible';

                    document.getElementById(nomeRaccoonlvl3).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl3).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl3).style.visibility = 'hidden';

                    document.getElementById(nomeCobralvl3).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl3).alt = 'morreu';
                    document.getElementById(nomeBatnlvl3).alt = 'morreu';

                    document.getElementById(nomeSardaonlvl3).alt = 'morreu';
                    document.getElementById(nomeMochonlvl3).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl3).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl3).alt = 'morreu';
                    document.getElementById(nomeAguialvl3).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl3).alt = 'morreu';
                }
                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';

                desSardao3 = setInterval(mudaSardao3, 200);
                moveSardao3 = setInterval(deslocaSardao3, 40);
                desDodo = setInterval(mudaDodo, 200);
                moveDodo = setInterval(deslocaDodo, 40);
                desPassaro = setInterval(mudaPassaro, 200);
                movePassaro = setInterval(deslocaPassaro, 40);
            }
        }
        if (nlvl === 9 || nlvl === 10) {
            maxVida = 750;
            vidaAtual = 750;
            quantidadeMaxVida.style.width = maxVida + 'px';
            quantidadeDeVida.style.width = vidaAtual + "px";
            if (nlvl === 10) {
                document.getElementById('ninja_preto').style.visibility = 'visible';

                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';
                for (var n10 = 1; n10 <= 3; n10++) {
                    var nomeSardaonlvl10 = 'sardao' + n10;
                    var nomeMochonlvl10 = 'mocho' + n10;
                    var nomeOuriconlvl10 = 'ourico' + n10;

                    var nomeSardao2nlvl10 = 'sardao2_' + n10;
                    var nomeCobralvl10 = 'cobra' + n10;
                    var nomeBatnlvl10 = 'bat' + n10;

                    var nomeSardao3nlvl10 = 'sardao3_' + n10;
                    var nomeDodolvl10 = 'dodo' + n10;
                    var nomePassaronlvl10 = 'passaro' + n10;

                    var nomeAguialvl10 = 'aguia' + n10;
                    var nomeCrocodilolvl10 = 'crocodilo' + n10;
                    var nomeRaccoonlvl10 = 'raccoon' + n10;

                    document.getElementById(nomeSardaonlvl10).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl10).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl10).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl10).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl10).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl10).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl10).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl10).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl10).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl10).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl10).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl10).style.visibility = 'hidden';

                    document.getElementById(nomeSardaonlvl10).alt = 'morreu';
                    document.getElementById(nomeMochonlvl10).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl10).alt = 'morreu';

                    document.getElementById(nomeCobralvl10).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl10).alt = 'morreu';
                    document.getElementById(nomeBatnlvl10).alt = 'morreu';

                    document.getElementById(nomeDodolvl10).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl10).alt = 'morreu';
                    document.getElementById(nomePassaronlvl10).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl10).alt = 'morreu';
                    document.getElementById(nomeAguialvl10).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl10).alt = 'morreu';
                }
                desNinjaPreto = setInterval(mudaNinjaPreto, 200)
            }
            else {
                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                for (var n4 = 1; n4 <= 3; n4++) {
                    var nomeSardaonlvl4 = 'sardao' + n4;
                    var nomeMochonlvl4 = 'mocho' + n4;
                    var nomeOuriconlvl4 = 'ourico' + n4;

                    var nomeSardao2nlvl4 = 'sardao2_' + n4;
                    var nomeCobralvl4 = 'cobra' + n4;
                    var nomeBatnlvl4 = 'bat' + n4;

                    var nomeSardao3nlvl4 = 'sardao3_' + n4;
                    var nomeDodolvl4 = 'dodo' + n4;
                    var nomePassaronlvl4 = 'passaro' + n4;

                    var nomeAguialvl4 = 'aguia' + n4;
                    var nomeCrocodilolvl4 = 'crocodilo' + n4;
                    var nomeRaccoonlvl4 = 'raccoon' + n4;

                    document.getElementById(nomeSardaonlvl4).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl4).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl4).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl4).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl4).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl4).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl4).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl4).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl4).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl4).style.visibility = 'visible';
                    document.getElementById(nomeAguialvl4).style.visibility = 'visible';
                    document.getElementById(nomeCrocodilolvl4).style.visibility = 'visible';

                    document.getElementById(nomeSardaonlvl4).alt = 'morreu';
                    document.getElementById(nomeMochonlvl4).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl4).alt = 'morreu';

                    document.getElementById(nomeCobralvl4).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl4).alt = 'morreu';
                    document.getElementById(nomeBatnlvl4).alt = 'morreu';

                    document.getElementById(nomeDodolvl4).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl4).alt = 'morreu';
                    document.getElementById(nomePassaronlvl4).alt = 'morreu';
                }
                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';

                desRaccoon = setInterval(mudaRaccoon, 200);
                moveRaccoon = setInterval(deslocaRaccoon, 40);
                desAguia = setInterval(mudaAguia, 200);
                moveAguia = setInterval(deslocaAguia, 40);
                desCrocodilo = setInterval(mudaCrocodilo, 200);
                moveCrocodilo = setInterval(deslocaCrocodilo, 40);
            }
        }

        numNivel.innerHTML = '<h1>NÍVEL' + nlvl + '</h1>';
    } else {
        if (nlvl === 1 || nlvl === 2 || nlvl === 3 || nlvl === 4) {
            maxVida = 150;
            vidaAtual = 150;
            quantidadeMaxVida.style.width = maxVida + 'px';
            quantidadeDeVida.style.width = vidaAtual + "px";
            if (nlvl === 2) {
                document.getElementById('ninja_verde').style.visibility = 'visible';
                desNinjaVerde = setInterval(mudaNinjaVerde, 200);

                for (var n7 = 1; n7 <= 3; n7++) {
                    var nomeSardaonlvl7 = 'sardao' + n7;
                    var nomeMochonlvl7 = 'mocho' + n7;
                    var nomeOuriconlvl7 = 'ourico' + n7;

                    var nomeSardao2nlvl7 = 'sardao2_' + n7;
                    var nomeCobralvl7 = 'cobra' + n7;
                    var nomeBatnlvl7 = 'bat' + n7;

                    var nomeSardao3nlvl7 = 'sardao3_' + n7;
                    var nomeDodolvl7 = 'dodo' + n7;
                    var nomePassaronlvl7 = 'passaro' + n7;

                    var nomeAguialvl7 = 'aguia' + n7;
                    var nomeCrocodilolvl7 = 'crocodilo' + n7;
                    var nomeRaccoonlvl7 = 'raccoon' + n7;

                    document.getElementById(nomeSardaonlvl7).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl7).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl7).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl7).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl7).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl7).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl7).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl7).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl7).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl7).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl7).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl7).style.visibility = 'hidden';

                    document.getElementById(nomeSardaonlvl7).alt = 'morreu';
                    document.getElementById(nomeMochonlvl7).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl7).alt = 'morreu';

                    document.getElementById(nomeCobralvl7).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl7).alt = 'morreu';
                    document.getElementById(nomeBatnlvl7).alt = 'morreu';

                    document.getElementById(nomeDodolvl7).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl7).alt = 'morreu';
                    document.getElementById(nomePassaronlvl7).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl7).alt = 'morreu';
                    document.getElementById(nomeAguialvl7).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl7).alt = 'morreu';
                }
                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';
            }
            else if (nlvl === 1 || nlvl === 3) {
                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                for (var n1 = 1; n1 <= 3; n1++) {
                    var nomeSardaonlvl1 = 'sardao' + n1;
                    var nomeMochonlvl1 = 'mocho' + n1;
                    var nomeOuriconlvl1 = 'ourico' + n1;

                    var nomeSardao2nlvl1 = 'sardao2_' + n1;
                    var nomeCobralvl1 = 'cobra' + n1;
                    var nomeBatnlvl1 = 'bat' + n1;

                    var nomeSardao3nlvl1 = 'sardao3_' + n1;
                    var nomeDodolvl1 = 'dodo' + n1;
                    var nomePassaronlvl1 = 'passaro' + n1;

                    var nomeAguialvl1 = 'aguia' + n1;
                    var nomeCrocodilolvl1 = 'crocodilo' + n1;
                    var nomeRaccoonlvl1 = 'raccoon' + n1;

                    document.getElementById(nomeSardaonlvl1).style.visibility = 'visible';
                    document.getElementById(nomeMochonlvl1).style.visibility = 'visible';
                    document.getElementById(nomeOuriconlvl1).style.visibility = 'visible';

                    document.getElementById(nomeSardao2nlvl1).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl1).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl1).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl1).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl1).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl1).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl1).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl1).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl1).style.visibility = 'hidden';

                    document.getElementById(nomeCobralvl1).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl1).alt = 'morreu';
                    document.getElementById(nomeBatnlvl1).alt = 'morreu';

                    document.getElementById(nomeDodolvl1).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl1).alt = 'morreu';
                    document.getElementById(nomePassaronlvl1).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl1).alt = 'morreu';
                    document.getElementById(nomeAguialvl1).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl1).alt = 'morreu';
                }
                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                desSardao = setInterval(mudaSardao, 200);
                moveSardao = setInterval(deslocaSardao, 40);
                desMocho = setInterval(mudaMocho, 200);
                moveMocho = setInterval(deslocaMocho, 10);
                desOurico = setInterval(mudaOurico, 200);
                moveOurico = setInterval(deslocaOurico, 40);
            }
            if (nlvl === 4) {
                document.getElementById('ninja_azul').style.visibility = 'visible';
                desNinjaAzul = setInterval(mudaNinjaAzul, 200);

                for (var n9 = 1; n9 <= 3; n9++) {
                    var nomeSardaonlvl9 = 'sardao' + n9;
                    var nomeMochonlvl9 = 'mocho' + n9;
                    var nomeOuriconlvl9 = 'ourico' + n9;

                    var nomeSardao2nlvl9 = 'sardao2_' + n9;
                    var nomeCobralvl9 = 'cobra' + n9;
                    var nomeBatnlvl9 = 'bat' + n9;

                    var nomeSardao3nlvl9 = 'sardao3_' + n9;
                    var nomeDodolvl9 = 'dodo' + n9;
                    var nomePassaronlvl9 = 'passaro' + n9;

                    var nomeAguialvl9 = 'aguia' + n9;
                    var nomeCrocodilolvl9 = 'crocodilo' + n9;
                    var nomeRaccoonlvl9 = 'raccoon' + n9;

                    document.getElementById(nomeSardaonlvl9).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl9).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl9).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl9).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl9).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl9).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl9).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl9).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl9).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl9).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl9).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl9).style.visibility = 'hidden';

                    document.getElementById(nomeSardaonlvl9).alt = 'morreu';
                    document.getElementById(nomeMochonlvl9).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl9).alt = 'morreu';

                    document.getElementById(nomeCobralvl9).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl9).alt = 'morreu';
                    document.getElementById(nomeBatnlvl9).alt = 'morreu';

                    document.getElementById(nomeDodolvl9).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl9).alt = 'morreu';
                    document.getElementById(nomePassaronlvl9).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl9).alt = 'morreu';
                    document.getElementById(nomeAguialvl9).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl9).alt = 'morreu';
                }
                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';
            }
            else {
                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';
            }
        }
        if (nlvl === 5 || nlvl === 6) {
            maxVida = 350;
            vidaAtual = 350;
            quantidadeMaxVida.style.width = maxVida + 'px';
            quantidadeDeVida.style.width = vidaAtual + "px";
            if (nlvl === 6) {
                document.getElementById('ninja_roxo').style.visibility = 'visible';
                desNinjaRoxo = setInterval(mudaNinjaRoxo, 200);

                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';

                for (var n6 = 1; n6 <= 3; n6++) {

                    var nomeSardaonlvl6 = 'sardao' + n6;
                    var nomeMochonlvl6 = 'mocho' + n6;
                    var nomeOuriconlvl6 = 'ourico' + n6;

                    var nomeSardao2nlvl6 = 'sardao2_' + n6;
                    var nomeCobralvl6 = 'cobra' + n6;
                    var nomeBatnlvl6 = 'bat' + n6;

                    var nomeSardao3nlvl6 = 'sardao3_' + n6;
                    var nomeDodolvl6 = 'dodo' + n6;
                    var nomePassaronlvl6 = 'passaro' + n6;

                    var nomeAguialvl6 = 'aguia' + n6;
                    var nomeCrocodilolvl6 = 'crocodilo' + n6;
                    var nomeRaccoonlvl6 = 'raccoon' + n6;

                    document.getElementById(nomeSardaonlvl6).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl6).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl6).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl6).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl6).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl6).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl6).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl6).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl6).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl6).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl6).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl6).style.visibility = 'hidden';

                    document.getElementById(nomeSardaonlvl6).alt = 'morreu';
                    document.getElementById(nomeMochonlvl6).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl6).alt = 'morreu';

                    document.getElementById(nomeSardao2nlvl6).alt = 'morreu';
                    document.getElementById(nomeCobralvl6).alt = 'morreu';
                    document.getElementById(nomeBatnlvl6).alt = 'morreu';

                    document.getElementById(nomeDodolvl6).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl6).alt = 'morreu';
                    document.getElementById(nomePassaronlvl6).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl6).alt = 'morreu';
                    document.getElementById(nomeAguialvl6).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl6).alt = 'morreu';

                }
            }
            else {
                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                for (var n2 = 1; n2 <= 3; n2++) {

                    var nomeSardaonlvl2 = 'sardao' + n2;
                    var nomeMochonlvl2 = 'mocho' + n2;
                    var nomeOuriconlvl2 = 'ourico' + n2;

                    var nomeSardao2nlvl2 = 'sardao2_' + n2;
                    var nomeCobralvl2 = 'cobra' + n2;
                    var nomeBatnlvl2 = 'bat' + n2;

                    var nomeSardao3nlvl2 = 'sardao3_' + n2;
                    var nomeDodolvl2 = 'dodo' + n2;
                    var nomePassaronlvl2 = 'passaro' + n2;

                    var nomeAguialvl2 = 'aguia' + n2;
                    var nomeCrocodilolvl2 = 'crocodilo' + n2;
                    var nomeRaccoonlvl2 = 'raccoon' + n2;

                    document.getElementById(nomeSardaonlvl2).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl2).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl2).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl2).style.visibility = 'visible';
                    document.getElementById(nomeCobralvl2).style.visibility = 'visible';
                    document.getElementById(nomeBatnlvl2).style.visibility = 'visible';

                    document.getElementById(nomeDodolvl2).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl2).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl2).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl2).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl2).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl2).style.visibility = 'hidden';

                    document.getElementById(nomeSardaonlvl2).alt = 'morreu';
                    document.getElementById(nomeMochonlvl2).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl2).alt = 'morreu';

                    document.getElementById(nomeDodolvl2).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl2).alt = 'morreu';
                    document.getElementById(nomePassaronlvl2).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl2).alt = 'morreu';
                    document.getElementById(nomeAguialvl2).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl2).alt = 'morreu';

                }
                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';

                desCobra = setInterval(mudaCobra, 200);
                moveCobra = setInterval(deslocaCobra, 40);
                desBat = setInterval(mudaBat, 200);
                moveBat = setInterval(deslocaBat, 40);
                desSardao2 = setInterval(mudaSardao2, 200);
                moveSardao2 = setInterval(deslocaSardao2, 40);
            }
        }
        if (nlvl === 7 || nlvl === 8) {
            maxVida = 350;
            vidaAtual = 350;
            quantidadeMaxVida.style.width = maxVida + 'px';
            quantidadeDeVida.style.width = vidaAtual + "px";
            if (nlvl === 8) {
                document.getElementById('ninja_vermelho').style.visibility = 'visible';
                desNinjaVermelho = setInterval(mudaNinjaVermelho, 200);

                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';

                for (var n8 = 1; n8 <= 3; n8++) {
                    var nomeSardaonlvl8 = 'sardao' + n8;
                    var nomeMochonlvl8 = 'mocho' + n8;
                    var nomeOuriconlvl8 = 'ourico' + n8;

                    var nomeSardao2nlvl8 = 'sardao2_' + n8;
                    var nomeCobralvl8 = 'cobra' + n8;
                    var nomeBatnlvl8 = 'bat' + n8;

                    var nomeSardao3nlvl8 = 'sardao3_' + n8;
                    var nomeDodolvl8 = 'dodo' + n8;
                    var nomePassaronlvl8 = 'passaro' + n8;

                    var nomeAguialvl8 = 'aguia' + n8;
                    var nomeCrocodilolvl8 = 'crocodilo' + n8;
                    var nomeRaccoonlvl8 = 'raccoon' + n8;

                    document.getElementById(nomeSardaonlvl8).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl8).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl8).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl8).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl8).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl8).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl8).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl8).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl8).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl8).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl8).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl8).style.visibility = 'hidden';

                    document.getElementById(nomeCobralvl8).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl8).alt = 'morreu';
                    document.getElementById(nomeBatnlvl8).alt = 'morreu';

                    document.getElementById(nomeSardaonlvl8).alt = 'morreu';
                    document.getElementById(nomeMochonlvl8).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl8).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl8).alt = 'morreu';
                    document.getElementById(nomeAguialvl8).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl8).alt = 'morreu';

                    document.getElementById(nomeDodolvl8).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl8).alt = 'morreu';
                    document.getElementById(nomePassaronlvl8).alt = 'morreu';
                }
                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';
            }
            else {
                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                for (var n3 = 1; n3 <= 3; n3++) {
                    var nomeSardaonlvl3 = 'sardao' + n3;
                    var nomeMochonlvl3 = 'mocho' + n3;
                    var nomeOuriconlvl3 = 'ourico' + n3;

                    var nomeSardao2nlvl3 = 'sardao2_' + n3;
                    var nomeCobralvl3 = 'cobra' + n3;
                    var nomeBatnlvl3 = 'bat' + n3;

                    var nomeSardao3nlvl3 = 'sardao3_' + n3;
                    var nomeDodolvl3 = 'dodo' + n3;
                    var nomePassaronlvl3 = 'passaro' + n3;

                    var nomeAguialvl3 = 'aguia' + n3;
                    var nomeCrocodilolvl3 = 'crocodilo' + n3;
                    var nomeRaccoonlvl3 = 'raccoon' + n3;

                    document.getElementById(nomeSardaonlvl3).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl3).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl3).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl3).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl3).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl3).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl3).style.visibility = 'visible';
                    document.getElementById(nomeSardao3nlvl3).style.visibility = 'visible';
                    document.getElementById(nomePassaronlvl3).style.visibility = 'visible';

                    document.getElementById(nomeRaccoonlvl3).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl3).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl3).style.visibility = 'hidden';

                    document.getElementById(nomeCobralvl3).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl3).alt = 'morreu';
                    document.getElementById(nomeBatnlvl3).alt = 'morreu';

                    document.getElementById(nomeSardaonlvl3).alt = 'morreu';
                    document.getElementById(nomeMochonlvl3).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl3).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl3).alt = 'morreu';
                    document.getElementById(nomeAguialvl3).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl3).alt = 'morreu';
                }
                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';

                desSardao3 = setInterval(mudaSardao3, 200);
                moveSardao3 = setInterval(deslocaSardao3, 40);
                desDodo = setInterval(mudaDodo, 200);
                moveDodo = setInterval(deslocaDodo, 40);
                desPassaro = setInterval(mudaPassaro, 200);
                movePassaro = setInterval(deslocaPassaro, 40);
            }
        }
        if (nlvl === 9 || nlvl === 10) {
            maxVida = 750;
            vidaAtual = 750;
            quantidadeMaxVida.style.width = maxVida + 'px';
            quantidadeDeVida.style.width = vidaAtual + "px";
            if (nlvl === 10) {
                document.getElementById('ninja_preto').style.visibility = 'visible';

                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';
                for (var n10 = 1; n10 <= 3; n10++) {
                    var nomeSardaonlvl10 = 'sardao' + n10;
                    var nomeMochonlvl10 = 'mocho' + n10;
                    var nomeOuriconlvl10 = 'ourico' + n10;

                    var nomeSardao2nlvl10 = 'sardao2_' + n10;
                    var nomeCobralvl10 = 'cobra' + n10;
                    var nomeBatnlvl10 = 'bat' + n10;

                    var nomeSardao3nlvl10 = 'sardao3_' + n10;
                    var nomeDodolvl10 = 'dodo' + n10;
                    var nomePassaronlvl10 = 'passaro' + n10;

                    var nomeAguialvl10 = 'aguia' + n10;
                    var nomeCrocodilolvl10 = 'crocodilo' + n10;
                    var nomeRaccoonlvl10 = 'raccoon' + n10;

                    document.getElementById(nomeSardaonlvl10).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl10).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl10).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl10).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl10).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl10).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl10).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl10).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl10).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl10).style.visibility = 'hidden';
                    document.getElementById(nomeAguialvl10).style.visibility = 'hidden';
                    document.getElementById(nomeCrocodilolvl10).style.visibility = 'hidden';

                    document.getElementById(nomeSardaonlvl10).alt = 'morreu';
                    document.getElementById(nomeMochonlvl10).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl10).alt = 'morreu';

                    document.getElementById(nomeCobralvl10).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl10).alt = 'morreu';
                    document.getElementById(nomeBatnlvl10).alt = 'morreu';

                    document.getElementById(nomeDodolvl10).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl10).alt = 'morreu';
                    document.getElementById(nomePassaronlvl10).alt = 'morreu';

                    document.getElementById(nomeRaccoonlvl10).alt = 'morreu';
                    document.getElementById(nomeAguialvl10).alt = 'morreu';
                    document.getElementById(nomeCrocodilolvl10).alt = 'morreu';
                }
                desNinjaPreto = setInterval(mudaNinjaPreto, 200)
            }
            else {
                document.getElementById('ninja_preto').style.visibility = 'hidden';
                document.getElementById('ninja_preto').alt = 'morreu';

                for (var n4 = 1; n4 <= 3; n4++) {
                    var nomeSardaonlvl4 = 'sardao' + n4;
                    var nomeMochonlvl4 = 'mocho' + n4;
                    var nomeOuriconlvl4 = 'ourico' + n4;

                    var nomeSardao2nlvl4 = 'sardao2_' + n4;
                    var nomeCobralvl4 = 'cobra' + n4;
                    var nomeBatnlvl4 = 'bat' + n4;

                    var nomeSardao3nlvl4 = 'sardao3_' + n4;
                    var nomeDodolvl4 = 'dodo' + n4;
                    var nomePassaronlvl4 = 'passaro' + n4;

                    var nomeAguialvl4 = 'aguia' + n4;
                    var nomeCrocodilolvl4 = 'crocodilo' + n4;
                    var nomeRaccoonlvl4 = 'raccoon' + n4;

                    document.getElementById(nomeSardaonlvl4).style.visibility = 'hidden';
                    document.getElementById(nomeMochonlvl4).style.visibility = 'hidden';
                    document.getElementById(nomeOuriconlvl4).style.visibility = 'hidden';

                    document.getElementById(nomeSardao2nlvl4).style.visibility = 'hidden';
                    document.getElementById(nomeCobralvl4).style.visibility = 'hidden';
                    document.getElementById(nomeBatnlvl4).style.visibility = 'hidden';

                    document.getElementById(nomeDodolvl4).style.visibility = 'hidden';
                    document.getElementById(nomeSardao3nlvl4).style.visibility = 'hidden';
                    document.getElementById(nomePassaronlvl4).style.visibility = 'hidden';

                    document.getElementById(nomeRaccoonlvl4).style.visibility = 'visible';
                    document.getElementById(nomeAguialvl4).style.visibility = 'visible';
                    document.getElementById(nomeCrocodilolvl4).style.visibility = 'visible';

                    document.getElementById(nomeSardaonlvl4).alt = 'morreu';
                    document.getElementById(nomeMochonlvl4).alt = 'morreu';
                    document.getElementById(nomeOuriconlvl4).alt = 'morreu';

                    document.getElementById(nomeCobralvl4).alt = 'morreu';
                    document.getElementById(nomeSardao2nlvl4).alt = 'morreu';
                    document.getElementById(nomeBatnlvl4).alt = 'morreu';

                    document.getElementById(nomeDodolvl4).alt = 'morreu';
                    document.getElementById(nomeSardao3nlvl4).alt = 'morreu';
                    document.getElementById(nomePassaronlvl4).alt = 'morreu';
                }
                document.getElementById('ninja_roxo').style.visibility = 'hidden';
                document.getElementById('ninja_roxo').alt = 'morreu';

                document.getElementById('ninja_vermelho').style.visibility = 'hidden';
                document.getElementById('ninja_vermelho').alt = 'morreu';

                document.getElementById('ninja_verde').style.visibility = 'hidden';
                document.getElementById('ninja_verde').alt = 'morreu';

                document.getElementById('ninja_azul').style.visibility = 'hidden';
                document.getElementById('ninja_azul').alt = 'morreu';

                desRaccoon = setInterval(mudaRaccoon, 200);
                moveRaccoon = setInterval(deslocaRaccoon, 40);
                desAguia = setInterval(mudaAguia, 200);
                moveAguia = setInterval(deslocaAguia, 40);
                desCrocodilo = setInterval(mudaCrocodilo, 200);
                moveCrocodilo = setInterval(deslocaCrocodilo, 40);
            }
        }
    }
    varDeOpcao = false;
}

function mudaSapo() {
    sapo.src = "imagens/" + sapoSent + "/sapo" + nsapo + ".png";
    nsapo++;
    if (nsapo === 20)
        nsapo = 1;
}

// sardao 1

function mudaSardao() {
    for (var n = 1; n <= 3; n++) {
        var nome = 'sardao' + n;
        document.getElementById(nome).src = "imagens/sardao/sardao" + nsardao + ".png";
        nsardao++;
        if (nsardao === 10)
            nsardao = 6;
    }
}

function deslocaSardao() {
    for (var n = 1; n <= 3; n++) {
        var nome = 'sardao' + n;
        //----------PARA VERIFICAR A COLISAO MAIS VALE SER OS INIMIGOS DE ENCONTRO COM O SAPO DO Q O CONTRARIO--------//
        verificaColisao('jogador', 106, 112, 'sardao' + n, 61, 149);
        document.getElementById(nome).style.left = parseInt(document.getElementById(nome).style.left) - 7 + 'px';
    }
}


// sardao 2

function mudaSardao2() {
    for (var n = 1; n <= 3; n++) {
        var nomes = 'sardao2_' + n;
        document.getElementById(nomes).src = "imagens/sardao2/sardao2_" + nsardao2 + ".png";
        nsardao2++;
        if (nsardao2 === 5)
            nsardao2 = 1;
    }
}

function deslocaSardao2() {
    for (var n = 1; n <= 3; n++) {
        var nomes = 'sardao2_' + n;
        //----------PARA VERIFICAR A COLISAO MAIS VALE SER OS INIMIGOS DE ENCONTRO COM O SAPO DO Q O CONTRARIO--------//
        verificaColisao('jogador', 106, 112, 'sardao2_' + n, 61, 149);
        document.getElementById(nomes).style.left = parseInt(document.getElementById(nomes).style.left) - 7 + 'px';
    }
}

// sardao 3

function mudaSardao3() {
    for (var n = 1; n <= 3; n++) {
        var names = 'sardao3_' + n;
        document.getElementById(names).src = "imagens/sardao3/sardao3_" + nsardao3 + ".png";
        nsardao3++;
        if (nsardao3 === 5)
            nsardao3 = 1;
    }
}

function deslocaSardao3() {
    for (var n = 1; n <= 3; n++) {
        var names = 'sardao3_' + n;
        //----------PARA VERIFICAR A COLISAO MAIS VALE SER OS INIMIGOS DE ENCONTRO COM O SAPO DO Q O CONTRARIO--------//
        verificaColisao('jogador', 106, 112, 'sardao3_' + n, 61, 149);
        document.getElementById(names).style.left = parseInt(document.getElementById(names).style.left) - 7 + 'px';
    }
}


// mocho

function mudaMocho() {
    for (var y = 1; y <= 3; y++) {
        var name = 'mocho' + y;
        document.getElementById(name).src = "imagens/mocho/mocho" + nmocho + ".png";
        nmocho++;
        if (nmocho === 8)
            nmocho = 1;
    }

}

function deslocaMocho() {
    posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 1001 - 500);

    if (nlvl === 1 || nlvl === 2) {

        if ((parseInt(layer1.style.backgroundPositionX) + 1500 + parseInt(posicaoInicialAleatoriaInimigo)) <= parseInt(sapo.style.left)) {

            if (parseInt(document.getElementById("mocho1").style.left) >= 0) {
                document.getElementById("mocho1").style.left = parseInt(document.getElementById("mocho1").style.left) + 5 + 'px';
                document.getElementById("mocho1").style.top = parseInt(document.getElementById("mocho1").style.top) + 5 + 'px';
            } else
                document.getElementById("mocho1").style.left = parseInt(document.getElementById("mocho1").style.left) + 5 + 'px';
            verificaColisao('jogador', 106, 112, 'mocho1', 102, 127);

        }

        if ((parseInt(layer1.style.backgroundPositionX) + 4500 + parseInt(posicaoInicialAleatoriaInimigo)) <= parseInt(sapo.style.left)) {

            if (parseInt(document.getElementById("mocho2").style.left) >= 0) {
                document.getElementById("mocho2").style.left = parseInt(document.getElementById("mocho2").style.left) + 5 + 'px';
                document.getElementById("mocho2").style.top = parseInt(document.getElementById("mocho2").style.top) + 5 + 'px';
            } else
                document.getElementById("mocho2").style.left = parseInt(document.getElementById("mocho2").style.left) + 5 + 'px';
            verificaColisao('jogador', 106, 112, 'mocho2', 102, 127);

        }

        if ((parseInt(layer1.style.backgroundPositionX) + 7600 + parseInt(posicaoInicialAleatoriaInimigo)) <= parseInt(sapo.style.left)) {

            if (parseInt(document.getElementById("mocho3").style.left) >= 0) {
                document.getElementById("mocho3").style.left = parseInt(document.getElementById("mocho3").style.left) + 5 + 'px';
                document.getElementById("mocho3").style.top = parseInt(document.getElementById("mocho3").style.top) + 5 + 'px';
            } else {
                document.getElementById("mocho3").style.left = parseInt(document.getElementById("mocho3").style.left) + 5 + 'px';
                verificaColisao('jogador', 106, 112, 'mocho3', 102, 127);
            }
        }
    }
}

// ouriço
function mudaOurico() {
    for (var o = 1; o <= 3; o++) {
        var nombre = 'ourico' + o;
        document.getElementById(nombre).src = "imagens/ourico/ourico" + nourico + ".png";
        nourico++;
        if (nourico === 8)
            nourico = 1;
    }

}

function deslocaOurico() {
    for (var o = 1; o <= 3; o++) {
        var nombre = 'ourico' + o;
        document.getElementById(nombre).style.left = parseInt(document.getElementById(nombre).style.left) - 7 + 'px';
        verificaColisao('jogador', 106, 112, 'ourico' + o, 87, 130);
    }
}

// cobra
function mudaCobra() {
    for (var c = 1; c <= 3; c++) {
        var nomeCobra = 'cobra' + c;
        document.getElementById(nomeCobra).src = "imagens/cobra/move/cobra" + ncobra + ".png";
        ncobra++;
        if (ncobra === 7)
            ncobra = 1;
    }

}

function deslocaCobra() {
    for (var c = 1; c <= 3; c++) {
        var nomeCobra = 'cobra' + c;
        document.getElementById(nomeCobra).style.left = parseInt(document.getElementById(nomeCobra).style.left) - 7 + 'px';
        verificaColisao('jogador', 106, 112, 'cobra' + c, 129, 131);
    }
}

// morcego

function mudaBat() {
    for (var c = 1; c <= 3; c++) {
        var nomeBat = 'bat' + c;
        document.getElementById(nomeBat).src = "imagens/bat/bat" + nBat + ".png";
        nBat++;
        if (nBat === 3)
            nBat = 1;
    }

}

function deslocaBat() {
    for (var c = 1; c <= 3; c++) {
        var nomeBat = 'bat' + c;
        document.getElementById(nomeBat).style.left = parseInt(document.getElementById(nomeBat).style.left) - 7 + 'px';
        verificaColisao('jogador', 106, 112, 'bat' + c, 100, 114);
    }
}

// raccoon

function mudaRaccoon() {
    for (var ra = 1; ra <= 3; ra++) {
        var nomeRaccoon = 'raccoon' + ra;
        document.getElementById(nomeRaccoon).src = "imagens/raccoon/raccoon" + nraccoon + ".png";
        nraccoon++;
        if (nraccoon === 7)
            nraccoon = 1;
    }

}

function deslocaRaccoon() {
    for (var ra = 1; ra <= 3; ra++) {
        var nomeRaccoon = 'raccoon' + ra;
        document.getElementById(nomeRaccoon).style.left = parseInt(document.getElementById(nomeRaccoon).style.left) - 7 + 'px';
        verificaColisao('jogador', 106, 112, 'raccoon' + ra, 150, 147.56);
    }
}

// aguia

function mudaAguia() {
    for (var a = 1; a <= 3; a++) {
        var nomeAguia = 'aguia' + a;
        document.getElementById(nomeAguia).src = "imagens/aguia/move/aguia" + naguia + ".png";
        naguia++;
        if (naguia === 8)
            naguia = 1;
    }
}

function deslocaAguia() {
    for (var a = 1; a <= 3; a++) {
        var nomeAguia = 'aguia' + a;
        document.getElementById(nomeAguia).style.left = parseInt(document.getElementById(nomeAguia).style.left) - 7 + 'px';
        verificaColisao('jogador', 106, 112, 'aguia' + a, 150, 150);
    }
}

// passaro

function mudaPassaro() {
    for (var a = 1; a <= 3; a++) {
        var nomePassaro = 'passaro' + a;
        document.getElementById(nomePassaro).src = "imagens/passaro/passaro" + nPassaro + ".png";
        nPassaro++;
        if (nPassaro === 3)
            nPassaro = 1;
    }
}

function deslocaPassaro() {
    for (var a = 1; a <= 3; a++) {
        var nomePassaro = 'passaro' + a;
        document.getElementById(nomePassaro).style.left = parseInt(document.getElementById(nomePassaro).style.left) - 7 + 'px';
        verificaColisao('jogador', 106, 112, 'passaro' + a, 110, 139.19);
    }
}

// dodo

function mudaDodo() {
    for (var d = 1; d <= 3; d++) {
        var nomeDodo = 'dodo' + d;
        document.getElementById(nomeDodo).src = "imagens/dodo/dodo" + ndodo + ".png";
        ndodo++;
        if (ndodo === 6)
            ndodo = 1;
    }

}

function deslocaDodo() {
    for (var d = 1; d <= 3; d++) {
        var nomeDodo = 'dodo' + d;
        document.getElementById(nomeDodo).style.left = parseInt(document.getElementById(nomeDodo).style.left) - 7 + 'px';
        verificaColisao('jogador', 106, 112, 'dodo' + d, 105, 100.73);
    }
}

// crocodilo

function mudaCrocodilo() {
    for (var croc = 1; croc <= 3; croc++) {
        var nomeCrocodilo = 'crocodilo' + croc;
        document.getElementById(nomeCrocodilo).src = "imagens/crocodilo/crocodilo" + ncrocodilo + ".png";
        ncrocodilo++;
        if (ncrocodilo === 9)
            ncrocodilo = 1;
    }

}

function deslocaCrocodilo() {
    for (var croc = 1; croc <= 3; croc++) {
        var nomeCrocodilo = 'crocodilo' + croc;
        document.getElementById(nomeCrocodilo).style.left = parseInt(document.getElementById(nomeCrocodilo).style.left) - 7 + 'px';
        verificaColisao('jogador', 106, 112, 'crocodilo' + croc, 55.59, 150);
    }
}

function mudaNinjaVerde() {
    document.getElementById('ninja_verde').src = 'imagens/ninja_verde/ninja' + nNinjaVerde + '.png';
    nNinjaVerde++;
    if (nNinjaVerde === 7) {
        nNinjaVerde = 1;
    }
    verificaColisao('jogador', 106, 112, 'ninja_verde', 300, 300);
}

function mudaNinjaAzul() {
    document.getElementById('ninja_azul').src = 'imagens/ninja_azul/ninja' + nNinjaAzul + '.png';
    nNinjaAzul++;
    if (nNinjaAzul === 7) {
        nNinjaAzul = 1;
    }
    verificaColisao('jogador', 106, 112, 'ninja_azul', 300, 300);
}

function mudaNinjaVermelho() {
    document.getElementById('ninja_vermelho').src = 'imagens/ninja_vermelho/ninja' + nNinjaVermelho + '.png';
    nNinjaVermelho++;
    if (nNinjaVermelho === 7) {
        nNinjaVermelho = 1;
    }
    verificaColisao('jogador', 106, 112, 'ninja_vermelho', 300, 300);
}

function mudaNinjaRoxo() {
    document.getElementById('ninja_roxo').src = 'imagens/ninja_roxo/ninja' + nNinjaRoxo + '.png';
    nNinjaRoxo++;
    if (nNinjaRoxo === 7) {
        nNinjaRoxo = 1;
    }
    verificaColisao('jogador', 106, 112, 'ninja_roxo', 300, 300);
}

function mudaNinjaPreto() {
    document.getElementById('ninja_preto').src = 'imagens/ninja_preto/ninja' + nNinjaPreto + '.png';
    nNinjaPreto++;
    if (nNinjaPreto === 7) {
        nNinjaPreto = 1;
    }
    verificaColisao('jogador', 106, 112, 'ninja_preto', 300, 300);
}

//------------ANIMACAO SAPO MODO BATALHA..........--------------//
function deslocaSapoEmBatalha() {
    if (parseInt(sapo.style.left) > 50) {
        posicaoBatalhaSapoAnimacao = -posicaoBatalhaSapoAnimacao;
    }
    if (parseInt(sapo.style.left) < 10) {
        clearInterval(deslocaSapoBatalhaAnimacao);
        posicaoBatalhaSapoAnimacao = 5;
    }
    sapo.style.left = parseInt(sapo.style.left) + posicaoBatalhaSapoAnimacao + 'px';
}

function deslocaInimigo() {
    // console.log(parseInt(document.getElementById(auxilioNomeInimigo).style.left));
    if (parseInt(document.getElementById(auxilioNomeInimigo).style.left) < 400)
        posicaoBatalhaInimigo = -posicaoBatalhaInimigo;
    if (parseInt(document.getElementById(auxilioNomeInimigo).style.left) >= 550) {
        clearInterval(deslocaBatalhaInimigoAnimacao);
        posicaoBatalhaInimigo = 5;
    }
    document.getElementById(auxilioNomeInimigo).style.left = parseInt(document.getElementById(auxilioNomeInimigo).style.left) - posicaoBatalhaInimigo + 'px';

}