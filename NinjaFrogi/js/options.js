//---------------------COLOCAR EM MODO PAUSA JOGO-------------------------------//
function verificaPause(recebida) {
    var tecla = recebida.key;

//x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
//------------------Variaveis do som---------------------//
    if (tecla === 'm') {
        contaMutes++;
        if (contaMutes % 2 !== 0) {
            somFundo1.muted = true;
            somSaltar.muted = true;
            somModoBatalha.muted = true;
            somPerdeVidaOuHP.muted = true;
            somGanhaVidaOuXP.muted = true;
            somMudarSeta.muted = true;
        } else {
            somFundo1.muted = false;
            somSaltar.muted = false;
            somModoBatalha.muted = false;
            somPerdeVidaOuHP.muted = false;
            somGanhaVidaOuXP.muted = false;
            somMudarSeta.muted = false;

        }
    }

//x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
    if (pausado === false && tecla === 'p' || pausado === false && tecla === 'o') {
        console.log('pq n funcionas?: ' + auxilioBatalha === false);
        pausado = true;
        //maria------------------------------//
        play_pausa.src = "imagens/pause_icon-icons.com_50078.png";
        //

        clearInterval(desSardao);
        clearInterval(moveSardao);
        clearInterval(desMocho);
        clearInterval(moveMocho);
        clearInterval(desOurico);
        clearInterval(moveOurico);
        //-.-.-.-.-.-.-.-.-.-.-.Acrescentei isto novo--.-.-.-.-.-.-.-.-.//
        clearInterval(desCobra);
        clearInterval(moveCobra);
        clearInterval(desRaccoon);
        clearInterval(moveRaccoon);
        clearInterval(desAguia);
        clearInterval(moveAguia);
        clearInterval(desCrocodilo);
        clearInterval(moveCrocodilo);
        clearInterval(desDodo);
        clearInterval(moveDodo);
        clearInterval(desSardao2);
        clearInterval(moveSardao2);
        clearInterval(desSardao3);
        clearInterval(moveSardao3);
        clearInterval(desBat);
        clearInterval(moveBat);
        clearInterval(desPassaro);
        clearInterval(movePassaro);
        //--------------------
        clearInterval(desNinjaPreto);
        clearInterval(desNinjaAzul);
        clearInterval(desNinjaRoxo);
        clearInterval(desNinjaVerde);
        clearInterval(desNinjaVermelho);
        console.log('pausa dentro do verificaPause: ' + pausado);
        if (tecla === 'o') {
            varDeOpcao = true;
            play_pausa.src = "imagens/play-png-image-54101.png";//maria
            console.log('entra aqui dentro');
            menuOpcoes();
            clearInterval(intSapo);

            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            //--------------------
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaVermelho);


        }
    } else {
        //-----------------------//
        if (pausado === true && tecla === 'p' && auxilioBatalha === false) {
            pausado = false;
            //maria------------------------------//
            play_pausa.src = "imagens/play-png-image-54101.png";
            desSardao = setInterval(mudaSardao, 200);
            moveSardao = setInterval(deslocaSardao, 40);
            desMocho = setInterval(mudaMocho, 200);
            moveMocho = setInterval(deslocaMocho, 10);
            moveOurico = setInterval(mudaOurico, 200);
            desOurico = setInterval(deslocaOurico, 40);
            desCobra = setInterval(mudaCobra, 200);
            moveCobra = setInterval(deslocaCobra, 40);
            desRaccoon = setInterval(mudaRaccoon, 200);
            moveRaccoon = setInterval(deslocaRaccoon, 40);
            desAguia = setInterval(mudaAguia, 200);
            moveAguia = setInterval(deslocaAguia, 40);
            desCrocodilo = setInterval(mudaCrocodilo, 200);
            moveCrocodilo = setInterval(deslocaCrocodilo, 40);
            desDodo = setInterval(mudaDodo, 200);
            moveDodo = setInterval(deslocaDodo, 40);
            desSardao2 = setInterval(mudaSardao, 200);
            moveSardao2 = setInterval(deslocaSardao2, 40);
            desSardao3 = setInterval(mudaSardao3, 200);
            moveSardao3 = setInterval(deslocaSardao3, 40);
            //--------
            desNinjaPreto = setInterval(mudaNinjaPreto, 200);
            desNinjaVermelho = setInterval(mudaNinjaVermelho, 200);
            desNinjaVerde = setInterval(mudaNinjaVerde, 200);
            desNinjaRoxo = setInterval(mudaNinjaRoxo, 200);
            desNinjaAzul = setInterval(mudaNinjaAzul, 200);
        }
    }
}


//-----------------------------MENU OPCOES---------------------------------//
function menuOpcoes() {

    var logo;
    logo = document.getElementById("logo_ecramenuprincipal");
    logo.style.visibility = "hidden";


    console.log('dentro menu de opcoes');
    ecraJogo.style.display = 'none';
    ecraInicial.style.display = 'block';
    jogar_btn.style.visibility = 'hidden';
    opcoes_btn.style.visibility = 'hidden';
    opcoes.style.visibility = 'visible';
    opcoes.innerHTML = '<img id="fechar_btn" src="imagens/botoes/fechar_btn.png">';
    opcoes.innerHTML += '<h1>Controlos</h1>';
    opcoes.innerHTML += '<table><tr><th>Mover</th><td><img src="imagens/botoes/teclaEsq.png"><img style="margin-left: 14px" src="imagens/botoes/teclaDir.png"></td></tr><tr><th>Saltar</th><td><img src="imagens/botoes/teclaEspaco.png"></td></tr><tr><th>Pausar o jogo</th><td><img src="imagens/botoes/teclaP.png"></td></tr><tr><th>Ligar/Desligar o som</th><td><img src="imagens/botoes/teclaM.png"></td></tr><tr><th>Menu de opções</th><td><img src="imagens/botoes/teclaO.png"></td></tr></table>';
    fechar_btn = document.getElementById('fechar_btn');

    fechar_btn.onmouseover = function () {
        fechar_btn.src = 'imagens/botoes/fecharHover_btn.png';
        fechar_btn.style.cursor = 'pointer';
    };

    fechar_btn.onmouseout = function () {
        fechar_btn.src = 'imagens/botoes/fechar_btn.png';
    };

    fechar_btn.onclick = function () {
        if (varDeOpcao === true) {
            logo.style.visibility = "visible";
            opcoes.style.visibility = 'hidden';
            ecraInicial.style.display = 'none';
            ecraJogo.style.display = 'initial';
            carregaJogo();
        } else {
            logo.style.visibility = "visible";
            jogar_btn.style.visibility = 'visible';
            opcoes_btn.style.visibility = 'visible';
            opcoes.style.visibility = 'hidden';
        }
    };
}

console.log('alturaEcra: ' + innerHeight);
console.log('larguraEcra: ' + innerWidth);