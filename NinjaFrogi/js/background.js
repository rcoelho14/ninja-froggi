// ------------------------- escolha de backgroun consoante level ------------------------------

function background() {
    //--------------------nivel 1 e 2-------------------------------------
    if (parseInt(nlvl) < 3) {

        backgrounds.style.backgroundColor = "deepskyblue";

        //layer1
        layer1.style.backgroundImage = "url('imagens/background/bg1/bg1_layer1.png')";
        layer1.style.zIndex = -1;

        //layer2
        layer2.style.backgroundImage = "url('imagens/background/bg1/bg1_layer2.png')";
        layer2.style.zIndex = -2;

        //layer3
        layer3.style.backgroundImage = "url('imagens/background/bg1/bg1_layer3.png')";
        layer3.style.zIndex = -3;

        //layer4
        layer4.style.backgroundImage = "url('imagens/background/bg1/bg1_layer4.png')";
        layer4.style.zIndex = -4;

        //layer5
        layer5.style.backgroundImage = "url('imagens/background/bg1/bg1_layer5.png')";
        layer5.style.zIndex = -5;

    }

    //--------------------nivel 3 e 4-------------------------------------
    if (parseInt(nlvl) >= 3 && parseInt(nlvl) < 5) {

        backgrounds.style.backgroundColor = "deepskyblue";

        //layer1
        layer1.style.backgroundImage = "url('imagens/background/bg2/bg2_layer1.png')";
        layer1.style.zIndex = -1;

        //layer2
        layer2.style.backgroundImage = "url('imagens/background/bg2/bg2_layer2.png')";
        layer2.style.zIndex = -2;

        //layer3
        layer3.style.backgroundImage = "url('imagens/background/bg2/bg2_layer3.png')";
        layer3.style.zIndex = -3;

        //layer4
        layer4.style.backgroundImage = "url('imagens/background/bg2/bg2_layer4.png')";
        layer4.style.zIndex = -4;

        //layer5
        layer5.style.backgroundImage = "url('imagens/background/bg2/bg2_layer5.png')";
        layer5.style.zIndex = -5;

    }
    //--------------------nivel 5 e 6-------------------------------------
    if (parseInt(nlvl) >= 5 && parseInt(nlvl) < 7) {

        backgrounds.style.backgroundColor = "937F61";

        //layer1
        layer1.style.backgroundImage = "url('imagens/background/bg3/bg3_layer1.png')";
        layer1.style.zIndex = 2;

        //layer2
        layer2.style.backgroundImage = "url('imagens/background/bg3/bg3_layer2.png')";
        layer2.style.zIndex = 1;
        layer2.style.opacity = 0.5;

        //layer3
        layer3.style.backgroundImage = "url('imagens/background/bg3/bg3_layer3.png')";
        layer3.style.zIndex = -3;

        //layer4
        layer4.style.backgroundImage = "url('imagens/background/bg3/bg3_layer4.png')";
        layer4.style.zIndex = -4;

        //layer5
        layer5.style.backgroundImage = "url('imagens/background/bg3/bg3_layer5.png')";
        layer5.style.zIndex = -5;

        //layer6
        layer6.style.backgroundImage = "url('imagens/background/bg3/bg3_layer6.png')";
        layer6.style.zIndex = -6;

        //layer7
        layer7.style.backgroundImage = "url('imagens/background/bg3/bg3_layer7.png')";
        layer7.style.zIndex = -7;

        //layer8
        layer8.style.backgroundImage = "url('imagens/background/bg3/bg3_layer8.png')";
        layer8.style.zIndex = -8;
    }

    //--------------------nivel 7 e 8-------------------------------------
    if (parseInt(nlvl) >= 7 && parseInt(nlvl) < 9) {

        backgrounds.style.backgroundColor = "0F1727";

        //layer1
        layer1.style.backgroundImage = "url('imagens/background/bg4/bg4_layer1.png')";
        layer1.style.zIndex = 2;
        layer1.style.opacity = 0.5;

        //layer2
        layer2.style.backgroundImage = "url('imagens/background/bg4/bg4_layer2.png')";
        layer2.style.zIndex = -1;

        //layer3
        layer3.style.backgroundImage = "url('imagens/background/bg4/bg4_layer3.png')";
        layer3.style.zIndex = -3;

        //layer4
        layer4.style.backgroundImage = "url('imagens/background/bg4/bg4_layer4.png')";
        layer4.style.zIndex = -4;

        //layer5
        layer5.style.backgroundImage = "url('imagens/background/bg4/bg4_layer5.png')";
        layer5.style.zIndex = -5;

        //layer6
        layer6.style.backgroundImage = "url('imagens/background/bg4/bg4_layer6.png')";
        layer6.style.zIndex = -6;

        //layer7
        layer7.style.backgroundImage = "url('imagens/background/bg4/bg4_layer7.png')";
        layer7.style.zIndex = -7;

        //layer8
        layer8.style.backgroundImage = "none";
        layer8.style.zIndex = -8;
    }
    //--------------------nivel 9 e 10-------------------------------------
    if (parseInt(nlvl) >= 9 && parseInt(nlvl) < 11) {

        backgrounds.style.backgroundColor = "deepskyblue";

        //layer1
        layer1.style.backgroundImage = "url('imagens/background/bg5/bg5_layer1.png')";
        layer1.style.zIndex = -1;

        //layer2
        layer2.style.backgroundImage = "url('imagens/background/bg5/bg5_layer2.png')";
        layer2.style.zIndex = -2;

        //layer3
        layer3.style.backgroundImage = "url('imagens/background/bg5/bg5_layer3.png')";
        layer3.style.zIndex = -3;

        //layer4
        layer4.style.backgroundImage = "url('imagens/background/bg5/bg5_layer4.png')";
        layer4.style.zIndex = -4;

        //layer5
        layer5.style.backgroundImage = "url('imagens/background/bg5/bg5_layer5.png')";
        layer5.style.zIndex = -5;

        //layer6
        layer6.style.backgroundImage = "url('imagens/background/bg5/bg5_layer6.png')";
        layer6.style.zIndex = -6;

        //layer7
        layer7.style.backgroundImage = "none";
        layer7.style.zIndex = -7;

        //layer
        layer8.style.backgroundImage = "none";
        layer8.style.zIndex = -8;
    }
}


//-------------------------PARALAX DOS BACKGROUNDS---------------//
function mudaLayer() {


    //--------------------------PEDRA FINAL-----------------------------------------
    if (parseInt(sapo.style.left) + 112 < 400) {
        document.getElementById(rockish).style.left = parseInt(document.getElementById(rockish).style.left) - (numLayer + 5) + 'px';
    }
    else {
        document.getElementById(rockish).style.left = parseInt(document.getElementById(rockish).style.left) - (numLayer + 10) + 'px';
    }


    //---------------------------OBJETOS-------------------------------------------

// lixo-----
    for (t = 1; t <= 3; t++) {

        var trash = 'garbage' + t;

        if (parseInt(sapo.style.left) + 112 < 400) {
            document.getElementById(trash).style.left = parseInt(document.getElementById(trash).style.left) - (numLayer + 5) + 'px';
        }
        else {
            document.getElementById(trash).style.left = parseInt(document.getElementById(trash).style.left) - (numLayer + 10) + 'px';
        }

    }

// pedras----

    for (var p = 1; p <= 3; p++) {

        var pedra = 'rock' + p;

        if (parseInt(sapo.style.left) + 112 < 400) {
            document.getElementById(pedra).style.left = parseInt(document.getElementById(pedra).style.left) - (numLayer + 5) + 'px';
        }
        else {
            document.getElementById(pedra).style.left = parseInt(document.getElementById(pedra).style.left) - (numLayer + 10) + 'px';
        }

    }

// troncos-----
    for (var ar = 1; ar <= 5; ar++) {

        var arvore = 'branch' + ar;

        if (parseInt(sapo.style.left) + 112 < 400) {
            document.getElementById(arvore).style.left = parseInt(document.getElementById(arvore).style.left) - (numLayer + 5) + 'px';
        }
        else {
            document.getElementById(arvore).style.left = parseInt(document.getElementById(arvore).style.left) - (numLayer + 10) + 'px';
        }
    }

// buracos-----
    for (var ho = 1; ho <= 2; ho++) {

        var buh = 'hole' + ho;

        if (parseInt(sapo.style.left) + 112 < 400) {
            document.getElementById(buh).style.left = parseInt(document.getElementById(buh).style.left) - (numLayer + 5) + 'px';
        }
        else {
            document.getElementById(buh).style.left = parseInt(document.getElementById(buh).style.left) - (numLayer + 10) + 'px';
        }
    }


// insetos--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //azul------------
    for (var bl = 1; bl <= 10; bl++) {

        var azul = 'azul' + bl;

        if (parseInt(sapo.style.left) + 112 < 400) {
            document.getElementById(azul).style.left = parseInt(document.getElementById(azul).style.left) - (numLayer + 5) + 'px';
        }
        else {
            document.getElementById(azul).style.left = parseInt(document.getElementById(azul).style.left) - (numLayer + 10) + 'px';
        }
    }
    //verde-----------
    for (var vrd = 1; vrd <= 5; vrd++) {

        var verde = 'verde' + vrd;

        if (parseInt(sapo.style.left) + 112 < 400) {
            document.getElementById(verde).style.left = parseInt(document.getElementById(verde).style.left) - (numLayer + 5) + 'px';
        }
        else {
            document.getElementById(verde).style.left = parseInt(document.getElementById(verde).style.left) - (numLayer + 10) + 'px';
        }
    }
    //dourado---------

    var dourado = 'dourado';

    if (parseInt(sapo.style.left) + 112 < 400) {
        document.getElementById(dourado).style.left = parseInt(document.getElementById(dourado).style.left) - (numLayer + 5) + 'px';
    }
    else {
        document.getElementById(dourado).style.left = parseInt(document.getElementById(dourado).style.left) - (numLayer + 10) + 'px';
    }

//------------------------------------------------------------------------------------------------------------------------------------------------------------
// ---------------------------BACKGROUNDS--------------------------------------------------------------------------------

    //---------------Background 1---------------------

    if (nlvl === 1 || nlvl === 2) {
        if (parseInt(sapo.style.left) + 112 < 400) {
            layer1.style.backgroundPositionX = parseInt(layer1.style.backgroundPositionX) - (numLayer + 5) + 'px';
            layer2.style.backgroundPositionX = parseInt(layer2.style.backgroundPositionX) - (numLayer + 5) * 0.80 + 'px';
            layer3.style.backgroundPositionX = parseInt(layer3.style.backgroundPositionX) - (numLayer + 5) * 0.1 + 'px';
            layer4.style.backgroundPositionX = parseInt(layer4.style.backgroundPositionX) - (numLayer + 5) * 0.15 + 'px';
            layer5.style.backgroundPositionX = parseInt(layer5.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
        } else {
            layer1.style.backgroundPositionX = parseInt(layer1.style.backgroundPositionX) - (numLayer + 10) + 'px';
            layer2.style.backgroundPositionX = parseInt(layer2.style.backgroundPositionX) - (numLayer + 10) * 0.80 + 'px';
            layer3.style.backgroundPositionX = parseInt(layer3.style.backgroundPositionX) - (numLayer + 10) * 0.1 + 'px';
            layer4.style.backgroundPositionX = parseInt(layer4.style.backgroundPositionX) - (numLayer + 10) * 0.15 + 'px';
            layer5.style.backgroundPositionX = parseInt(layer5.style.backgroundPositionX) - (numLayer + 10) * 0.115 + 'px';
        }
    }
    //---------------Background 2---------------------
    if (nlvl === 3 || nlvl === 4) {
        if (parseInt(sapo.style.left) + 112 < 400) {
            layer1.style.backgroundPositionX = parseInt(layer1.style.backgroundPositionX) - (numLayer + 5) + 'px';
            layer2.style.backgroundPositionX = parseInt(layer2.style.backgroundPositionX) - (numLayer + 5) * 0.80 + 'px';
            layer3.style.backgroundPositionX = parseInt(layer3.style.backgroundPositionX) - (numLayer + 5) * 0.1 + 'px';
            layer4.style.backgroundPositionX = parseInt(layer4.style.backgroundPositionX) - (numLayer + 5) * 0.15 + 'px';
            layer5.style.backgroundPositionX = parseInt(layer5.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
        } else {
            layer1.style.backgroundPositionX = parseInt(layer1.style.backgroundPositionX) - (numLayer + 10) + 'px';
            layer2.style.backgroundPositionX = parseInt(layer2.style.backgroundPositionX) - (numLayer + 10) * 0.80 + 'px';
            layer3.style.backgroundPositionX = parseInt(layer3.style.backgroundPositionX) - (numLayer + 10) * 0.1 + 'px';
            layer4.style.backgroundPositionX = parseInt(layer4.style.backgroundPositionX) - (numLayer + 10) * 0.15 + 'px';
            layer5.style.backgroundPositionX = parseInt(layer5.style.backgroundPositionX) - (numLayer + 30) * 0.115 + 'px';

        }

    }
    //---------------Background 3---------------------
    if (nlvl === 5 || nlvl === 6) {
        if (parseInt(sapo.style.left) + 112 < 400) {
            layer1.style.backgroundPositionX = parseInt(layer1.style.backgroundPositionX) - (numLayer + 5) + 'px';
            layer2.style.backgroundPositionX = parseInt(layer2.style.backgroundPositionX) - (numLayer + 5) * 0.80 + 'px';
            layer3.style.backgroundPositionX = parseInt(layer3.style.backgroundPositionX) - (numLayer + 5) * 0.1 + 'px';
            layer4.style.backgroundPositionX = parseInt(layer4.style.backgroundPositionX) - (numLayer + 5) * 0.15 + 'px';
            layer5.style.backgroundPositionX = parseInt(layer5.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
            layer6.style.backgroundPositionX = parseInt(layer6.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
            layer7.style.backgroundPositionX = parseInt(layer7.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
            layer8.style.backgroundPositionX = parseInt(layer8.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
        } else {
            layer1.style.backgroundPositionX = parseInt(layer1.style.backgroundPositionX) - (numLayer + 10) + 'px';
            layer2.style.backgroundPositionX = parseInt(layer2.style.backgroundPositionX) - (numLayer + 10) * 0.80 + 'px';
            layer3.style.backgroundPositionX = parseInt(layer3.style.backgroundPositionX) - (numLayer + 10) * 0.1 + 'px';
            layer4.style.backgroundPositionX = parseInt(layer4.style.backgroundPositionX) - (numLayer + 10) * 0.15 + 'px';
            layer5.style.backgroundPositionX = parseInt(layer5.style.backgroundPositionX) - (numLayer + 10) * 0.115 + 'px';
            layer6.style.backgroundPositionX = parseInt(layer6.style.backgroundPositionX) - (numLayer + 10) * 0.115 + 'px';
            layer7.style.backgroundPositionX = parseInt(layer7.style.backgroundPositionX) - (numLayer + 10) * 0.115 + 'px';
            layer8.style.backgroundPositionX = parseInt(layer8.style.backgroundPositionX) - (numLayer + 10) * 0.115 + 'px';
        }

    }
    //---------------Background 4---------------------
    if (nlvl === 7 || nlvl === 8) {
        if (parseInt(sapo.style.left) + 112 < 400) {
            layer1.style.backgroundPositionX = parseInt(layer1.style.backgroundPositionX) - (numLayer + 5) + 'px';
            layer2.style.backgroundPositionX = parseInt(layer2.style.backgroundPositionX) - (numLayer + 5) * 0.80 + 'px';
            layer3.style.backgroundPositionX = parseInt(layer3.style.backgroundPositionX) - (numLayer + 5) * 0.1 + 'px';
            layer4.style.backgroundPositionX = parseInt(layer4.style.backgroundPositionX) - (numLayer + 5) * 0.15 + 'px';
            layer5.style.backgroundPositionX = parseInt(layer5.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
            layer6.style.backgroundPositionX = parseInt(layer6.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
            layer7.style.backgroundPositionX = parseInt(layer7.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
        } else {
            layer1.style.backgroundPositionX = parseInt(layer1.style.backgroundPositionX) - (numLayer + 10) + 'px';
            layer2.style.backgroundPositionX = parseInt(layer2.style.backgroundPositionX) - (numLayer + 10) * 0.80 + 'px';
            layer3.style.backgroundPositionX = parseInt(layer3.style.backgroundPositionX) - (numLayer + 10) * 0.1 + 'px';
            layer4.style.backgroundPositionX = parseInt(layer4.style.backgroundPositionX) - (numLayer + 10) * 0.15 + 'px';
            layer5.style.backgroundPositionX = parseInt(layer5.style.backgroundPositionX) - (numLayer + 10) * 0.115 + 'px';
            layer6.style.backgroundPositionX = parseInt(layer6.style.backgroundPositionX) - (numLayer + 10) * 0.115 + 'px';
            layer7.style.backgroundPositionX = parseInt(layer7.style.backgroundPositionX) - (numLayer + 10) * 0.115 + 'px';
        }

    }
    //---------------Background 5---------------------
    if (nlvl === 9 || nlvl === 10) {
        if (parseInt(sapo.style.left) + 112 < 400) {
            layer1.style.backgroundPositionX = parseInt(layer1.style.backgroundPositionX) - (numLayer + 5) + 'px';
            layer2.style.backgroundPositionX = parseInt(layer2.style.backgroundPositionX) - (numLayer + 5) * 0.80 + 'px';
            layer3.style.backgroundPositionX = parseInt(layer3.style.backgroundPositionX) - (numLayer + 5) * 0.1 + 'px';
            layer4.style.backgroundPositionX = parseInt(layer4.style.backgroundPositionX) - (numLayer + 5) * 0.15 + 'px';
            layer5.style.backgroundPositionX = parseInt(layer5.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
            layer6.style.backgroundPositionX = parseInt(layer6.style.backgroundPositionX) - (numLayer + 5) * 0.115 + 'px';
        } else {
            layer1.style.backgroundPositionX = parseInt(layer1.style.backgroundPositionX) - (numLayer + 10) + 'px';
            layer2.style.backgroundPositionX = parseInt(layer2.style.backgroundPositionX) - (numLayer + 10) * 0.80 + 'px';
            layer3.style.backgroundPositionX = parseInt(layer3.style.backgroundPositionX) - (numLayer + 10) * 0.1 + 'px';
            layer4.style.backgroundPositionX = parseInt(layer4.style.backgroundPositionX) - (numLayer + 10) * 0.15 + 'px';
            layer5.style.backgroundPositionX = parseInt(layer5.style.backgroundPositionX) - (numLayer + 10) * 0.115 + 'px';
            layer6.style.backgroundPositionX = parseInt(layer6.style.backgroundPositionX) - (numLayer + 10) * 0.115 + 'px';
        }
    }
}