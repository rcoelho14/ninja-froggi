//----------------------------DETECTAR COLISAO-------------------------------//
function verificaColisao(elemento1, elemento1Altura, elemento1Largura, elemento2, elemento2Altura, elemento2Largura) {
    var el1 = document.getElementById(elemento1);
    var el2 = document.getElementById(elemento2);
    if (document.getElementById(elemento2).alt !== 'morreu') {
        if (parseInt(el1.style.left) < parseInt(el2.style.left) + elemento2Largura && parseInt(el1.style.left) + elemento1Largura > parseInt(el2.style.left) && parseInt(el1.style.top) < parseInt(el2.style.top) + elemento2Altura && elemento1Altura + parseInt(el1.style.top) > parseInt(el2.style.top)) {
            colideCom(elemento2);
            return true;
        }
    }

}

function colideCom(objecto) {

//x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
//------------------Variaveis do som---------------------//
    somMudarSeta.pause();
    somSaltar.pause();
    somFundo1.pause();
    somGanhaVidaOuXP.pause();
    somPerdeVidaOuHP.pause();
    somModoBatalha.play();
    if (nlvl === 1 || nlvl === 2 || nlvl === 3 || nlvl === 4 || nlvl === 5 || nlvl === 6 || nlvl === 7 || nlvl === 8 || nlvl === 9 || nlvl === 10) {
        if (objecto === 'sardao1' || objecto === 'sardao2' || objecto === 'sardao3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'sardao1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 550 + 'px';
                vidaInimigo = 35;
                auxilioNomeInimigo = 'sardao1';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 25;
            } else if (objecto === 'sardao2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 550 + 'px';
                vidaInimigo = 35;
                auxilioNomeInimigo = 'sardao2';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 25;
            } else if (objecto === 'sardao3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 550 + 'px';
                vidaInimigo = 35;
                auxilioNomeInimigo = 'sardao3';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 25;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'sardao2_1' || objecto === 'sardao2_2' || objecto === 'sardao2_3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'sardao2_1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 550 + 'px';
                vidaInimigo = 120;
                auxilioNomeInimigo = 'sardao2_1';
                ataqueInimigo1 = 50;
                ataqueInimigo2 = 30;
            } else if (objecto === 'sardao2_2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 550 + 'px';
                vidaInimigo = 120;
                auxilioNomeInimigo = 'sardao2_2';
                ataqueInimigo1 = 50;
                ataqueInimigo2 = 30;
            } else if (objecto === 'sardao2_3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 550 + 'px';
                vidaInimigo = 120;
                auxilioNomeInimigo = 'sardao2_3';
                ataqueInimigo1 = 50;
                ataqueInimigo2 = 30;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'sardao3_1' || objecto === 'sardao3_2' || objecto === 'sardao3_3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'sardao3_1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 550 + 'px';
                vidaInimigo = 35;
                auxilioNomeInimigo = 'sardao3_1';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 25;
            } else if (objecto === 'sardao3_2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 550 + 'px';
                vidaInimigo = 35;
                auxilioNomeInimigo = 'sardao3_2';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 25;
            } else if (objecto === 'sardao3_3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 550 + 'px';
                vidaInimigo = 35;
                auxilioNomeInimigo = 'sardao3_3';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 25;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'mocho1' || objecto === 'mocho2' || objecto === 'mocho3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'mocho1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 60;
                auxilioNomeInimigo = 'mocho1';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 25;
            } else if (objecto === 'mocho2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 60;
                auxilioNomeInimigo = 'mocho2';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 25;
            } else if (objecto === 'mocho3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 60;
                auxilioNomeInimigo = 'mocho3';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 25;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'ourico1' || objecto === 'ourico2' || objecto === 'ourico3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'ourico1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 40;
                auxilioNomeInimigo = 'ourico1';
                ataqueInimigo1 = 40;
                ataqueInimigo2 = 30;
            } else if (objecto === 'ourico2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 40;
                auxilioNomeInimigo = 'ourico2';
                ataqueInimigo1 = 40;
                ataqueInimigo2 = 30;
            } else if (objecto === 'ourico3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 40;
                auxilioNomeInimigo = 'ourico3';
                ataqueInimigo1 = 40;
                ataqueInimigo2 = 30;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'cobra1' || objecto === 'cobra2' || objecto === 'cobra3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'cobra1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 175;
                auxilioNomeInimigo = 'cobra1';
                ataqueInimigo1 = 60;
                ataqueInimigo2 = 30;
            } else if (objecto === 'cobra2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 175;
                auxilioNomeInimigo = 'cobra2';
                ataqueInimigo1 = 60;
                ataqueInimigo2 = 30;
            } else if (objecto === 'cobra3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 175;
                auxilioNomeInimigo = 'cobra3';
                ataqueInimigo1 = 60;
                ataqueInimigo2 = 30;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'raccoon1' || objecto === 'raccoon2' || objecto === 'raccoon3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'raccoon1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 100;
                auxilioNomeInimigo = 'raccoon1';
                ataqueInimigo1 = 40;
                ataqueInimigo2 = 30;
            } else if (objecto === 'raccoon2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 100;
                auxilioNomeInimigo = 'raccoon2';
                ataqueInimigo1 = 40;
                ataqueInimigo2 = 30;
            } else if (objecto === 'raccoon3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 100;
                auxilioNomeInimigo = 'raccoon3';
                ataqueInimigo1 = 40;
                ataqueInimigo2 = 30;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'bat1' || objecto === 'bat2' || objecto === 'bat3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'bat1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 100;
                auxilioNomeInimigo = 'bat1';
                ataqueInimigo1 = 40;
                ataqueInimigo2 = 30;
            } else if (objecto === 'bat2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 100;
                auxilioNomeInimigo = 'bat2';
                ataqueInimigo1 = 40;
                ataqueInimigo2 = 30;
            } else if (objecto === 'bat3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 100;
                auxilioNomeInimigo = 'bat3';
                ataqueInimigo1 = 40;
                ataqueInimigo2 = 30;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'aguia1' || objecto === 'aguia2' || objecto === 'aguia3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'aguia1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 135;
                auxilioNomeInimigo = 'aguia1';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 35;
            } else if (objecto === 'aguia2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 135;
                auxilioNomeInimigo = 'aguia2';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 35;
            } else if (objecto === 'aguia3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 135;
                auxilioNomeInimigo = 'aguia3';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 35;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'passaro1' || objecto === 'passaro2' || objecto === 'passaro3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'passaro1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 135;
                auxilioNomeInimigo = 'passaro1';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 35;
            } else if (objecto === 'passaro2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 135;
                auxilioNomeInimigo = 'passaro2';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 35;
            } else if (objecto === 'passaro3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 135;
                auxilioNomeInimigo = 'passaro3';
                ataqueInimigo1 = 45;
                ataqueInimigo2 = 35;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'dodo1' || objecto === 'dodo2' || objecto === 'dodo3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'dodo1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 250;
                auxilioNomeInimigo = 'dodo1';
                ataqueInimigo1 = 80;
                ataqueInimigo2 = 70;
            } else if (objecto === 'dodo2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 250;
                auxilioNomeInimigo = 'dodo2';
                ataqueInimigo1 = 80;
                ataqueInimigo2 = 70;
            } else if (objecto === 'dodo3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 250;
                auxilioNomeInimigo = 'dodo3';
                ataqueInimigo1 = 80;
                ataqueInimigo2 = 70;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'crocodilo1' || objecto === 'crocodilo2' || objecto === 'crocodilo3') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            if (objecto === 'crocodilo1') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 350;
                auxilioNomeInimigo = 'crocodilo1';
                ataqueInimigo1 = 95;
                ataqueInimigo2 = 35;
            } else if (objecto === 'crocodilo2') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 350;
                auxilioNomeInimigo = 'crocodilo2';
                ataqueInimigo1 = 95;
                ataqueInimigo2 = 35;
            } else if (objecto === 'crocodilo3') {
                auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
                document.getElementById(objecto).style.left = 500 + 'px';
                vidaInimigo = 350;
                auxilioNomeInimigo = 'crocodilo3';
                ataqueInimigo1 = 95;
                ataqueInimigo2 = 35;
            }
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'ninja_verde') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
            document.getElementById(objecto).style.left = 500 + 'px';
            vidaInimigo = 75;
            auxilioNomeInimigo = 'ninja_verde';
            ataqueInimigo1 = 50;
            ataqueInimigo2 = 25;
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'ninja_azul') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
            document.getElementById(objecto).style.left = 500 + 'px';
            vidaInimigo = 75;
            auxilioNomeInimigo = 'ninja_azul';
            ataqueInimigo1 = 50;
            ataqueInimigo2 = 25;
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'ninja_vermelho') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
            document.getElementById(objecto).style.left = 500 + 'px';
            vidaInimigo = 200;
            auxilioNomeInimigo = 'ninja_vermelho';
            ataqueInimigo1 = 70;
            ataqueInimigo2 = 30;
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'ninja_roxo') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
            document.getElementById(objecto).style.left = 500 + 'px';
            vidaInimigo = 200;
            auxilioNomeInimigo = 'ninja_roxo';
            ataqueInimigo1 = 70;
            ataqueInimigo2 = 30;
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
        if (objecto === 'ninja_preto') {
            modoBatalha.style.visibility = 'visible';
            setaOpcao.style.visibility = 'visible';
            auxilioBatalha = true;
            pausado = true;
            auxilioPosicaoSapo = parseInt(sapo.style.left);
            auxilioPosicaoInimigo = parseInt(document.getElementById(objecto).style.left);
            document.getElementById(objecto).style.left = 500 + 'px';
            vidaInimigo = 500;
            auxilioNomeInimigo = 'ninja_preto';
            ataqueInimigo1 = 90;
            ataqueInimigo2 = 30;
            sapo.style.left = posicaoInicial + 'px';
            clearInterval(intSapo);
            clearInterval(desSardao);
            clearInterval(moveSardao);
            clearInterval(desMocho);
            clearInterval(moveMocho);
            clearInterval(desOurico);
            clearInterval(moveOurico);
            clearInterval(desCobra);
            clearInterval(moveCobra);
            clearInterval(desRaccoon);
            clearInterval(moveRaccoon);
            clearInterval(desAguia);
            clearInterval(moveAguia);
            clearInterval(desCrocodilo);
            clearInterval(moveCrocodilo);
            clearInterval(desDodo);
            clearInterval(moveDodo);
            clearInterval(desSardao2);
            clearInterval(moveSardao2);
            clearInterval(desSardao3);
            clearInterval(moveSardao3);
            clearInterval(desBat);
            clearInterval(moveBat);
            clearInterval(desPassaro);
            clearInterval(movePassaro);
            clearInterval(desNinjaPreto);
            clearInterval(desNinjaVerde);
            clearInterval(desNinjaRoxo);
            clearInterval(desNinjaAzul);
            clearInterval(desNinjaVermelho);
        }
    }
}

function rodaRamosPedras() {
    for (var a = 1; a <= 5; a++) {
        colideComRamoOuPedra('jogador', 90, 112, 'branch' + a, 200);
    }
    for (var r = 1; r <= 3; r++) {
        colideComRamoOuPedra('jogador', 90, 112, 'rock' + r, 193)
    }
}

function colideComRamoOuPedra(elemento1, elemento1Altura, elemento1Largura, elemento2, elemento2Largura) {
    var el1 = document.getElementById(elemento1);
    var el2 = document.getElementById(elemento2);
    for (var t = 1; t <= 5; t++) {
        var nomeRamo = 'branch' + t;
        if (nomeRamo == elemento2) {
            if (parseInt(parseInt(el1.style.top) + elemento1Altura) < parseInt(el2.style.top) && parseInt(parseInt(el1.style.left) + elemento1Largura / 2) > parseInt(el2.style.left) && parseInt(el1.style.left) < parseInt(parseInt(el2.style.left) + elemento2Largura)) {
                clearInterval(saltitaTimer);
                clearInterval(saltinhos);
                clearInterval(saltaLayer);
                //console.log(':::RAMO:::: ' + elemento2);
                //console.log('vamos la ver o num do numLayer: ' + numLayer);
                el1.style.top = '255px';
                //console.log(parseInt(el1.style.left) + ' -|||- ' + parseInt(parseInt(el2.style.left) + elemento2Largura));
                if (parseInt(parseInt(el2.style.left) + elemento2Largura) <= parseInt(parseInt(el1.style.left) + 10)) {
                    el1.style.top = 400 + 'px';
                    aSaltar = false;
                    delete saltitaTimer;
                    delete saltinhos;
                    /*velocidadezinhaTop = 5;
                     vamosLaVer = setInterval(desceSapo, 5);

                     function desceSapo() {
                     el1.style.top = parseInt(el1.style.top) + velocidadezinhaTop + 'px';
                     if (parseInt(el1.style.top) > 400)
                     clearInterval(vamosLaVer);
                     }*/
                }
                if (parseInt(parseInt(el1.style.left) + 30) < parseInt(parseInt(el2.style.left))) {
                    el1.style.top = 400 + 'px';
                    aSaltar = false;
                    delete saltitaTimer;
                    delete saltinhos;
                    /*velocidadezinhaTop = 5;
                     vamosLaVer = setInterval(desceSapo2, 5);

                     function desceSapo2() {
                     el1.style.top = parseInt(el1.style.top) + velocidadezinhaTop + 'px';
                     if (parseInt(el1.style.top) > 400)
                     clearInterval(vamosLaVer);
                     }*/
                }
            }
        }
    }
    for (var p = 1; p <= 3; p++) {
        var nomePedra = 'rock' + p;
        if (nomePedra == elemento2) {
            if (parseInt(parseInt(el1.style.top) + elemento1Altura) < parseInt(el2.style.top) && parseInt(parseInt(el1.style.left) + elemento1Largura / 2) > parseInt(el2.style.left) && parseInt(el1.style.left) < parseInt(parseInt(el2.style.left) + elemento2Largura)) {
                clearInterval(saltitaTimer);
                clearInterval(saltinhos);
                clearInterval(saltaLayer);
                //console.log(':::RAMO:::: ' + elemento2);
                //console.log('vamos la ver o num do numLayer: ' + numLayer);
                el1.style.top = '280px';
                //console.log(parseInt(el1.style.left) + ' -|||- ' + parseInt(parseInt(el2.style.left) + elemento2Largura));
                if (parseInt(parseInt(el2.style.left) + elemento2Largura) <= parseInt(parseInt(el1.style.left) + 10)) {
                    el1.style.top = 400 + 'px';
                    aSaltar = false;
                    delete saltitaTimer;
                    delete saltinhos;
                    /*velocidadezinhaTop = 5;
                     vamosLaVer = setInterval(desceSapo3, 5);

                     function desceSapo3() {
                     el1.style.top = parseInt(el1.style.top) + velocidadezinhaTop + 'px';
                     if (parseInt(el1.style.top) > 400)
                     clearInterval(vamosLaVer);
                     }*/
                }
                if (parseInt(parseInt(el1.style.left) + 30) < parseInt(parseInt(el2.style.left))) {
                    el1.style.top = 400 + 'px';
                    aSaltar = false;
                    delete saltitaTimer;
                    delete saltinhos;
                    /*velocidadezinhaTop = 5;
                     vamosLaVer = setInterval(desceSapo4, 5);

                     function desceSapo4() {
                     el1.style.top = parseInt(el1.style.top) + velocidadezinhaTop + 'px';
                     if (parseInt(el1.style.top) > 400)
                     clearInterval(vamosLaVer);
                     }*/
                }
            }
        }
    }

}

function rodaInsectosLixosBuracos() {
    for (var g = 1; g <= 3; g++) {
        colideComInsectoOuLixoOuBuraco('jogador', 90, 112, 'garbage' + g, 70, 96);
    }
    for (var ins = 1; ins <= 10; ins++) {
        //-.-.-.-.-.-.Dei 90 90 (e nao 100) ao insecto pq é uma bola e tao dar a sensacao q toca numa bola e nao num quadrado-.-.-//
        colideComInsectoOuLixoOuBuraco('jogador', 90, 112, 'azul' + ins, 90, 90);
    }
    for (var ver = 1; ver <= 5; ver++) {
        colideComInsectoOuLixoOuBuraco('jogador', 90, 112, 'verde' + ver, 40, 40);
    }
    for (var bu = 1; bu <= 2; bu++) {
        colideComInsectoOuLixoOuBuraco('jogador', 106, 112, 'hole' + bu, 70, 100);
    }
    colideComInsectoOuLixoOuBuraco('jogador', 90, 112, 'dourado', 40, 40);
    colideComInsectoOuLixoOuBuraco('jogador', 90, 112, 'endrock', 251, 336);

}

function colideComInsectoOuLixoOuBuraco(elemento1, elemento1Altura, elemento1Largura, elemento2, elemento2Altura, elemento2Largura) {
    var el1 = document.getElementById(elemento1);
    var el2 = document.getElementById(elemento2);
    if (document.getElementById(elemento2).alt !== 'ups') {
        if (parseInt(el1.style.left) < parseInt(el2.style.left) + elemento2Largura && parseInt(el1.style.left) + elemento1Largura > parseInt(el2.style.left) && parseInt(el1.style.top) < parseInt(el2.style.top) + elemento2Altura && elemento1Altura + parseInt(el1.style.top) > parseInt(el2.style.top)) {
            vamosVerQualInsectoOuQualLixoOuQualBuraco(elemento2);
            return true;
        }
    }
}

function vamosVerQualInsectoOuQualLixoOuQualBuraco(elemento2) {
    for (var ins = 1; ins <= 10; ins++) {
        var nomeInsectoAzul = 'azul' + ins;
        if (nomeInsectoAzul == elemento2) {
            //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
            //------------------Variaveis do som---------------------//
            somFundo1.play();
            somGanhaVidaOuXP.play();
            somMudarSeta.pause();
            somSaltar.pause();
            somPerdeVidaOuHP.pause();

            //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//

            console.log('yumiii :D');
            document.getElementById(elemento2).style.visibility = 'hidden';
            document.getElementById(elemento2).alt = 'ups';
            pontuacao += 10;
            divpontuacao.innerHTML = '<h1>' + pontuacao + '</h1>';
        }
    }
    for (var ver = 1; ver <= 5; ver++) {
        var nomeInsectoVerde = 'verde' + ver;
        if (nomeInsectoVerde == elemento2) {
            //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
            //------------------Variaveis do som---------------------//
            somMudarSeta.pause();
            somSaltar.pause();
            somFundo1.play();
            somPerdeVidaOuHP.pause();
            somGanhaVidaOuXP.play();

            //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
            console.log('yumiii :D');
            document.getElementById(elemento2).style.visibility = 'hidden';
            document.getElementById(elemento2).alt = 'ups';
            vidaAtual += 20;
            quantidadeDeVida.style.width = vidaAtual + "px";
        }
    }
    for (var gar = 1; gar <= 3; gar++) {
        var lix = 'garbage' + gar;
        if (lix == elemento2) {
            //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
            //------------------Variaveis do som-HELIA--------------------//

            somMudarSeta.pause();
            somSaltar.pause();
            somFundo1.play();
            somPerdeVidaOuHP.pause();
            somPerdeVidaOuHP.play();

            //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//

            console.log('bhlack!!');
            document.getElementById(elemento2).style.visibility = 'hidden';
            document.getElementById(elemento2).alt = 'ups';
            vidaAtual -= 10;
            quantidadeDeVida.style.width = vidaAtual + "px";
        }
    }

    for (var buz = 1; buz <= 2; buz++) {
        var buraquinho = 'hole' + buz;
        if (elemento2 == buraquinho) {
            document.getElementById(elemento2).style.visibility = 'hidden';
            document.getElementById(elemento2).alt = 'ups';
            contVidas--;
            if (contVidas === 2) {
                document.getElementById('vida3Img').style.visibility = 'hidden';
            }
            if (contVidas === 1) {
                document.getElementById('vida2Img').style.visibility = 'hidden';
            }
            if (contVidas === 0) {
                document.getElementById('vida1Img').style.visibility = 'hidden';
                fimJogo();
            }

            console.log('there there!!');
            //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
            //------------------Variaveis do som---------------------//
            somMudarSeta.pause();
            somSaltar.pause();
            somFundo1.play();
            somPerdeVidaOuHP.pause();
            somPerdeVidaOuHP.play();
            //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//

        }
    }
    if (elemento2 == 'dourado') {
        document.getElementById(elemento2).style.visibility = 'hidden';
        document.getElementById(elemento2).alt = 'ups';
        if (contVidas === 2) {
            contVidas++;
            document.getElementById('vida3Img').style.visibility = 'visible';
        }
        if (contVidas === 1) {
            contVidas++;
            document.getElementById('vida2Img').style.visibility = 'visible';
        }
        //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
        //------------------Variaveis do som---------------------//

        somMudarSeta.pause();
        somSaltar.pause();
        somFundo1.play();
        somPerdeVidaOuHP.pause();
        somGanhaVidaOuXP.play();

        //x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
        console.log('yumiii :D');
    }
    if (elemento2 == 'endrock') {
        document.getElementById(elemento2).style.visibility = 'hidden';
        document.getElementById(elemento2).alt = 'ups';
        fazResetParaLvlUp();
    }
}

