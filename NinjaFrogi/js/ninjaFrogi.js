// ---------------------------- Variaveis ---------------------------
var landingPage;
var jogar_btn;
var som_btn;
var opcoes_btn;
var fechar_btn;
var somMute;
var posicaoInicial = 10;
var velocidade = 10;
var opcoes;
var sapo;
var sardao;
var mocho;
var intSapo;
var nsapo = 1;
var backgrounds;

var teclas = new Array();

var nsardao = 1;
var desSardao;
var moveSardao;

var nourico = 1;
var desOurico;
var moveOurico;
//
var ncobra = 1;
var desCobra;
var moveCobra;

var naguia = 1;
var desAguia;
var moveAguia;

var ndodo = 1;
var desDodo;
var moveDodo;

var ncrocodilo = 1;
var desCrocodilo;
var moveCrocodilo;

var nraccoon = 1;
var desRaccoon;
var moveRaccoon;
//----
var nBat = 1;
var desBat;
var moveBat;

var nsardao2 = 1;
var desSardao2;
var moveSardao2;

var nsardao3 = 1;
var desSardao3;
var moveSardao3;

var nPassaro = 1;
var desPassaro;
var movePassaro;

var nNinjaAzul = 1;
var desNinjaAzul;

var nNinjaPreto = 1;
var desNinjaPreto;

var nNinjaRoxo = 1;
var desNinjaRoxo;

var nNinjaVerde = 1;
var desNinjaVerde;

var nNinjaVermelho = 1;
var desNinjaVermelho;
//
var vidaAtual = 150;
var vidaInimigo;
var modoBatalha;
var setaOpcao;
var auxilioBatalha = false;
var auxilioNomeInimigo;
var auxilioPosicaoSapo;
var auxilioPosicaoInimigo;
var auxilioNumCliquesBatalha = 0;
var ataqueInimigo1;
var ataqueInimigo2;
var auxilioQualAtaqueInimigo;

var modoBatalhaSapoAnimacao;
var estaMenuBatalha = false;
var esperaAnimacaoBatalha;
var posicaoBatalhaSapoAnimacao = 5;
var deslocaSapoBatalhaAnimacao;

var deslocaBatalhaInimigoAnimacao;
var posicaoBatalhaInimigo = 5;

var nmocho = 1;
var desMocho;
var moveMocho;
var pausado = false;
var varDeOpcao = false;
var ecraJogo;
var teclaBaixo = false;
var personagens;
var corpo;
var sapoSent = '';

var maxVida = 150;
var quantidadeMaxVida;
var quantidadeDeVida;
var feedback;
var chanceFugir;
//
var numVidas;
var vidaBar;
var numNivel;
var divpontuacao;
var pontuacao;
var posicaoInicialAleatoriaInimigo;

var intLayer;
var aSaltar = false;
var velocidadeVertical = 10;
var auxilioExtras = 0;
var colidiuExtras = false;
//---------------------Layers--------------------------------------

var layer1;
var layer2;
var layer3;
var layer4;
var layer5;
var layer6;
var layer7;
var layer8;

var saltaLayer;

var rockish;
var garbage;
var rock;
var hole;
var branch;
//-------nlv Começa em zero, dps vai incrementando com o carregajogo();-----------//
var numLayer = 0;
var nlvl = 0;
var contVidas = 3;
//x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
//------------------Variaveis do som----------------//
var somFundo1;
var somSaltar;
var somModoBatalha;
var somPerdeVidaOuHP;
var somGanhaVidaOuXP;
var somMudarSeta;
var contaMutes = 0;
var changelevel;
// ------------------- Window Functions ----------------------------


window.onload = function () {
    carregaElementos();
    comecaJogar();
};
window.onkeydown = function (event) {
    processaTeclas(event);
    verificaPause(event);
    moveSeta(event);
};
window.onkeyup = function (event) {
    limpaTeclas(event);
    clearInterval(intSapo);
    clearInterval(intLayer);
    teclaBaixo = false;
};


// ---------------------------- FUNÇÕES ------------------------------

function carregaElementos() {
//x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//
//------------------Variaveis do som-HELIA--------------------//
    logo = document.getElementById("logo_menuprincipal");
    somFundo1 = new Audio();
    somFundo1.src = 'sons/fundo/lv1.mp3';
    somFundo1.play();
    somSaltar = new Audio();
    somSaltar.src = 'sons/saltar/salta.mp3';
    somSaltar.pause();
    somModoBatalha = new Audio();
    somModoBatalha.src = 'sons/batalhaanimais/batalhaanimais.mp3';
    somModoBatalha.pause();
    somPerdeVidaOuHP = new Audio();
    somPerdeVidaOuHP.src = 'sons/perdeEstados/perdeVidaHp.mp3';
    somPerdeVidaOuHP.pause();
    somGanhaVidaOuXP = new Audio();
    somGanhaVidaOuXP.src = 'sons/ganhaEstados/ganhaVidaHp.mp3';
    somGanhaVidaOuXP.pause();
    somMudarSeta = new Audio();
    somMudarSeta.src = 'sons/mudarSetas/mudaSeta.mp3';
    somMudarSeta.pause();
//x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.x.X.//

    personagens = document.getElementById('personagens');
    corpo = document.getElementById('corpo');

    backgrounds = document.getElementById("background");

    layer1 = document.getElementById("layer1");
    layer1.style.backgroundPositionX = 0;
    layer2 = document.getElementById("layer2");
    layer2.style.backgroundPositionX = 0;
    layer3 = document.getElementById("layer3");
    layer3.style.backgroundPositionX = 0;
    layer4 = document.getElementById("layer4");
    layer4.style.backgroundPositionX = 0;
    layer5 = document.getElementById("layer5");
    layer5.style.backgroundPositionX = 0;
    layer6 = document.getElementById("layer6");
    layer6.style.backgroundPositionX = 0;
    layer7 = document.getElementById("layer7");
    layer7.style.backgroundPositionX = 0;
    layer8 = document.getElementById("layer8");
    layer8.style.backgroundPositionX = 0;

    somMute = false;
    ecraInicial = document.getElementById('ecraInicial');
    ecraJogo = document.getElementById('ecraJogo');
    ecraJogo.style.display = 'none';
    landingPage = document.getElementById('landingPage');
    opcoes = document.getElementById('opcoes');
    opcoes.style.visibility = 'hidden';
    numVidas = document.getElementById('numVidas');
    vidaBar = document.getElementById('vidaBar');
    numNivel = document.getElementById('numNivel');
    divpontuacao = document.getElementById('pontuacao');

    modoBatalha = document.getElementById('modoBatalha');
    modoBatalha.style.visibility = 'hidden';
    setaOpcao = document.getElementById('setaOpcao');
    setaOpcao.style.visibility = 'hidden';
    setaOpcao.style.top = 540 + 'px';
    pontuacao = 0;
    vidaBar.innerHTML = '<h1>HP</h1>';
    vidaBar.innerHTML += '<div id="myProgress"><div id="myBar"></div></div>';

    quantidadeMaxVida = document.getElementById('myProgress');
    quantidadeMaxVida.style.width = maxVida + 'px';
    quantidadeDeVida = document.getElementById('myBar');
    quantidadeDeVida.style.width = vidaAtual + "px";
    feedback = document.getElementById('feedbackAtaque');
    feedback.style.position = 'absolute';
    chanceFugir = Math.random();
    chanceFalharAtaque = Math.random();
    //

    numVidas.innerHTML = '<h1>VIDAS</h1>';
    numVidas.innerHTML += '<img id="vida1Img" alt = "temvida" src="imagens/vida/sapo.png"  style="position: absolute">' + '<img id="vida2Img" alt = "temvida" src="imagens/vida/sapo.png"  style="position: absolute">' + '<img id="vida3Img" alt = "temvida" src="imagens/vida/sapo.png"  style="position: absolute">';
    divpontuacao.innerHTML = '<h1>' + pontuacao + '</h1>';
    numNivel.innerHTML = '<h1>NÍVEL' + nlvl + '</h1>';
    numNivel.style.color = "white";
    landingPage.innerHTML = '<img id="jogar_btn" src="imagens/botoes/jogar_btn.png"  style="position: absolute">';
    landingPage.innerHTML += '<img id="som_btn" src="imagens/botoes/somON_btn.png"  style="position: absolute">';
    landingPage.innerHTML += '<img id="opcoes_btn" src="imagens/botoes/opcoes_btn.png" style="position: absolute">';
    jogar_btn = document.getElementById('jogar_btn');
    opcoes_btn = document.getElementById('opcoes_btn');
    som_btn = document.getElementById('som_btn');
    som_btn.style.left = parseInt(window.innerWidth) - 78 + 'px';
    som_btn.style.top = parseInt(window.innerHeight) - 170 + 'px';
    jogar_btn.style.position="absolute";
    jogar_btn.style.top = '67%';
    jogar_btn.style.left = 240 + 'px';
    opcoes_btn.style.top = '67%';
    opcoes_btn.style.left = 540 + 'px';

    play_pausa = document.getElementById("pausa_play");
    historiadiv = document.getElementById("historiadiv");
    ecraInicial.style.display = "none";
    seta = document.getElementById("seta");
    seta.style.visibility = "visible";

    seta.onmouseover = function () {
        seta.src = "imagens/seta_hover.png";
        seta.style.cursor = "pointer";
    };
    seta.onmouseout = function () {
        seta.src = "imagens/seta.png"
    };
    seta.onclick = function () {
        historiadiv.style.display = "none";
        ecraInicial.style.display = "block";

    };

    play_pausa = document.getElementById("pausa_play");


    //-----------Objetos------------------------------------------------------------------------------------------------------------------------------------

    // ----- lixo ---------------

    for (var g = 1; g <= 3; g++) {
        garbage = 'garbage' + g;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 200);
        personagens.innerHTML += '<img id="' + garbage + '" alt="nojo" src="imagens/extras/lixo.png" height = "70" style="position: absolute">';
        document.getElementById(garbage).style.top = 600 - 90 - 70 + 'px';

        if (g === 1) {
            document.getElementById(garbage).style.left = 700 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (g === 2) {
            document.getElementById(garbage).style.left = 4000 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else {
            document.getElementById(garbage).style.left = 7800 + posicaoInicialAleatoriaInimigo + 'px';
        }
    }

    // ----- pedras ---------------

    for (var r = 1; r <= 3; r++) {
        rock = 'rock' + r;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 200);
        personagens.innerHTML += '<img id="' + rock + '" src="imagens/extras/pedra.png" height = "135" style="position: absolute">';
        document.getElementById(rock).style.top = 390 + 'px';

        if (r === 1) {
            document.getElementById(rock).style.left = 1600 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else if (r === 2) {
            document.getElementById(rock).style.left = 5000 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else {
            document.getElementById(rock).style.left = 7000 + posicaoInicialAleatoriaInimigo + 'px';
        }
    }

    // ----- troncos ---------------

    for (var b = 1; b <= 5; b++) {
        var branch = 'branch' + b;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 200);
        personagens.innerHTML += '<img id="' + branch + '" src="imagens/extras/tronco.png" width="200" style="position: absolute">';
        document.getElementById(branch).style.top = 600 - 250 + 'px';


        if (b === 1) {
            document.getElementById(branch).style.left = 1100 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (b === 2) {
            document.getElementById(branch).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else if (b === 3) {
            document.getElementById(branch).style.left = 3700 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else if (b === 4) {
            document.getElementById(branch).style.left = 6400 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else {
            document.getElementById(branch).style.left = 8700 + posicaoInicialAleatoriaInimigo + 'px';
        }
    }


    //--------------------------------------INSECTOS-----------------------------------------------------
    //--.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.ALT NOS INSECTOS-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.//
    for (var az = 1; az <= 10; az++) {
        var inseto_azul = 'azul' + az;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 401 - 200);
        personagens.innerHTML += '<img id="' + inseto_azul + '" alt="yummi" src="imagens/insetoazul/insetoazul.png" height="70" style="position: absolute">';
        document.getElementById(inseto_azul).style.top = 600 - 350 + 'px';


        if (az === 1) {
            document.getElementById(inseto_azul).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (az === 2) {
            document.getElementById(inseto_azul).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (az === 3) {
            document.getElementById(inseto_azul).style.left = 2500 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (az === 4) {
            document.getElementById(inseto_azul).style.left = 3100 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (az === 5) {
            document.getElementById(inseto_azul).style.left = 3800 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (az === 6) {
            document.getElementById(inseto_azul).style.left = 4900 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (az === 7) {
            document.getElementById(inseto_azul).style.left = 5500 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (az === 8) {
            document.getElementById(inseto_azul).style.left = 6100 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (az === 9) {
            document.getElementById(inseto_azul).style.left = 7850 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else {
            document.getElementById(inseto_azul).style.left = 9000 + posicaoInicialAleatoriaInimigo + 'px';
        }
    }


    for (var v = 1; v <= 5; v++) {
        var inseto_verde = 'verde' + v;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 401 - 200);
        personagens.innerHTML += '<img id="' + inseto_verde + '" alt="yummi"  src="imagens/insetoverde/insetoverde.png" height="70" style="position: absolute">';
        document.getElementById(inseto_verde).style.top = 600 - 350 + 'px';


        if (v === 1) {
            document.getElementById(inseto_verde).style.left = 1500 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (v === 2) {
            document.getElementById(inseto_verde).style.left = 3500 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (v === 3) {
            document.getElementById(inseto_verde).style.left = 4100 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (v === 4) {
            document.getElementById(inseto_verde).style.left = 6800 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else {
            document.getElementById(inseto_verde).style.left = 8350 + posicaoInicialAleatoriaInimigo + 'px';
        }
    }


    var inseto_dourado = 'dourado';
    posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 7700);
    personagens.innerHTML += '<img id="' + inseto_dourado + '" alt="yummi" src="imagens/insetodourado/insetodourado.png" height="70" style="position: absolute">';
    document.getElementById(inseto_dourado).style.top = 600 - 350 + 'px';
    document.getElementById(inseto_dourado).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';

//----------------------buraco---------------------------------
    for (var h = 1; h <= 2; h++) {
        var hole = 'hole' + h;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 401 - 200);
        personagens.innerHTML += '<img id="' + hole + '" alt="buraco" src="imagens/extras/hole.png" style="position: absolute">';
        document.getElementById(hole).style.top = 600 - 100 + "px";

        if (h === 1) {
            document.getElementById(hole).style.left = 2850 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else {
            document.getElementById(hole).style.left = 8550 + posicaoInicialAleatoriaInimigo + 'px';
        }

    }

    //------------pedra dos 10000 px -----------------------

    rockish = 'endrock';
    personagens.innerHTML += '<img id="' + rockish + '" src="imagens/extras/pedra1.png" height = "251" width="336" style="position: absolute">';
    document.getElementById(rockish).style.top = 600 - 90 - 251 + 'px';
    document.getElementById(rockish).style.left = 10000 + 'px';


    //----------------------Acrescentar os inimigos-------------------//
    for (var num = 1; num <= 3; num++) {

        //--------Background 1 e 2---------//
        personagens.innerHTML += '<img id="sardao' + num + '" src="imagens/sardao/sardao1.png" height="61" width="149" style="position: absolute">';
        personagens.innerHTML += '<img id="mocho' + num + '" src="imagens/mocho/mocho1.png" height="102" width="127" style="position: absolute">';
        personagens.innerHTML += '<img id="ourico' + num + '" src="imagens/ourico/ourico1.png" height="87" width="130" style="position: absolute">';

        //--------Background 3---------//
        personagens.innerHTML += '<img id="cobra' + num + '" src="imagens/cobra/move/cobra1.png" height="129" width="131" style="position: absolute">';
        personagens.innerHTML += '<img id="bat' + num + '" src="imagens/bat/bat1.png" height="100" width="114" style="position: absolute">';
        personagens.innerHTML += '<img id="sardao2_' + num + '" src="imagens/sardao2/sardao2_1.png" height="61" width="149" style="position: absolute">';

        //---------Background 4------//
        personagens.innerHTML += '<img id="passaro' + num + '" src="imagens/passaro/passaro1.png" height="110" width="139.19" style="position: absolute">';
        personagens.innerHTML += '<img id="sardao3_' + num + '" src="imagens/sardao3/sardao3_1.png" height="61" width="149" style="position: absolute">';
        personagens.innerHTML += '<img id="dodo' + num + '" src="imagens/dodo/dodo1.png" height="105" width="100.73" style="position: absolute">';

        //---------Background 5----------//
        personagens.innerHTML += '<img id="crocodilo' + num + '" src="imagens/crocodilo/crocodilo1.png" height="55.59" width="150" style="position: absolute">';
        personagens.innerHTML += '<img id="raccoon' + num + '" src="imagens/raccoon/raccoon1.png" height="150" width="147.56" style="position: absolute">';
        personagens.innerHTML += '<img id="aguia' + num + '" src="imagens/aguia/move/aguia1.png" height="150" width="150" style="position: absolute">';
    }
    //-------NINJAS--------------//
    personagens.innerHTML += '<img id="ninja_azul" alt="yummi" src="imagens/ninja_azul/ninja1.png" height="300" width="300" style="position: absolute">';
    document.getElementById('ninja_azul').style.top = 600 - 90 - 300 + 'px';
    document.getElementById('ninja_azul').style.left = 350 + 'px';
    personagens.innerHTML += '<img id="ninja_preto" alt="yummi" src="imagens/ninja_preto/ninja1.png" height="300" width="300" style="position: absolute">';
    document.getElementById('ninja_preto').style.top = 600 - 90 - 300 + 'px';
    document.getElementById('ninja_preto').style.left = 350 + 'px';
    personagens.innerHTML += '<img id="ninja_roxo" alt="yummi"src="imagens/ninja_roxo/ninja1.png" height="300" width="300" style="position: absolute">';
    document.getElementById('ninja_roxo').style.top = 600 - 90 - 300 + 'px';
    document.getElementById('ninja_roxo').style.left = 350 + 'px';
    personagens.innerHTML += '<img id="ninja_verde" alt="yummi" src="imagens/ninja_verde/ninja1.png" height="300" width="300" style="position: absolute">';
    document.getElementById('ninja_verde').style.top = 600 - 90 - 300 + 'px';
    document.getElementById('ninja_verde').style.left = 350 + 'px';
    personagens.innerHTML += '<img id="ninja_vermelho" alt="yummi" src="imagens/ninja_vermelho/ninja1.png" height="300" width="300" style="position: absolute">';
    document.getElementById('ninja_vermelho').style.top = 600 - 90 - 300 + 'px';
    document.getElementById('ninja_vermelho').style.left = 350 + 'px';
    //




//--------------BG 1 e 2----------------------------------------------------------------------------
    for (var n = 1; n <= 3; n++) {
        var nomeSardao = 'sardao' + n;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 601 - 300);
        document.getElementById(nomeSardao).style.top = 600 - 90 - 61 + 'px';
        document.getElementById(nomeSardao).alt = 'vivo';


        if (n === 1) {
            document.getElementById(nomeSardao).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (n === 2) {
            document.getElementById(nomeSardao).style.left = 3500 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else {
            document.getElementById(nomeSardao).style.left = 7200 + posicaoInicialAleatoriaInimigo + 'px';
        }


    }

    for (var y = 1; y <= 3; y++) {
        var nomeMocho = 'mocho' + y;
        document.getElementById(nomeMocho).style.top = 102 + 'px';
        document.getElementById(nomeMocho).alt = 'vivo';

        document.getElementById(nomeMocho).style.left = -150 + 'px';
    }

    for (var o = 1; o <= 3; o++) {
        var nomeOurico = 'ourico' + o;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 401 - 200);
        document.getElementById(nomeOurico).style.top = 600 - 90 - 87 + 'px';
        document.getElementById(nomeOurico).alt = 'vivo';


        if (o === 1) {
            document.getElementById(nomeOurico).style.left = 4000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (o === 2) {
            document.getElementById(nomeOurico).style.left = 6500 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else {
            document.getElementById(nomeOurico).style.left = 9000 + posicaoInicialAleatoriaInimigo + 'px';
        }

    }
//

//--------------BG3 ----------------------------------------------------------------------------
    for (var c = 1; c <= 3; c++) {
        var nomeCobra = 'cobra' + c;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 401 - 200);
        document.getElementById(nomeCobra).style.top = 600 - 90 - 129 + 'px';
        document.getElementById(nomeCobra).alt = 'vivo';


        if (c === 1) {
            document.getElementById(nomeCobra).style.left = 4000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (c === 2) {
            document.getElementById(nomeCobra).style.left = 6500 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else {
            document.getElementById(nomeCobra).style.left = 9000 + posicaoInicialAleatoriaInimigo + 'px';
        }

    }

    for (var sar = 1; sar <= 3; sar++) {
        var nomeSardao2 = 'sardao2_' + sar;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 601 - 300);
        document.getElementById(nomeSardao2).style.top = 600 - 90 - 61 + 'px';
        document.getElementById(nomeSardao2).alt = 'vivo';


        if (sar === 1) {
            document.getElementById(nomeSardao2).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (sar === 2) {
            document.getElementById(nomeSardao2).style.left = 3500 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else {
            document.getElementById(nomeSardao2).style.left = 7200 + posicaoInicialAleatoriaInimigo + 'px';
        }

    }

    for (var bat = 1; bat <= 3; bat++) {
        var nomeBat = 'bat' + bat;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 1001 - 500);
        document.getElementById(nomeBat).style.top = 102 + 'px';
        document.getElementById(nomeBat).alt = 'vivo';

        if (bat === 1) {
            document.getElementById(nomeBat).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (bat === 2) {
            document.getElementById(nomeBat).style.left = 5000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else {
            document.getElementById(nomeBat).style.left = 8100 + posicaoInicialAleatoriaInimigo + 'px';
        }
    }


//---------BG4----------------------------------------------------------------------------------------

    for (var sard = 1; sard <= 3; sard++) {
        var nomeSardao3 = 'sardao3_' + sard;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 601 - 300);
        document.getElementById(nomeSardao3).style.top = 600 - 90 - 61 + 'px';
        document.getElementById(nomeSardao3).alt = 'vivo';


        if (sard === 1) {
            document.getElementById(nomeSardao3).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (sard === 2) {
            document.getElementById(nomeSardao3).style.left = 3500 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else {
            document.getElementById(nomeSardao3).style.left = 7200 + posicaoInicialAleatoriaInimigo + 'px';
        }

    }


    for (var d = 1; d <= 3; d++) {
        var nomeDodo = 'dodo' + d;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 601 - 300);
        document.getElementById(nomeDodo).style.top = 600 - 90 - 105 + 'px';
        document.getElementById(nomeDodo).alt = 'vivo';

        if (d === 1) {
            document.getElementById(nomeDodo).style.left = 4000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (d === 2) {
            document.getElementById(nomeDodo).style.left = 6500 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else {
            document.getElementById(nomeDodo).style.left = 9000 + posicaoInicialAleatoriaInimigo + 'px';
        }
    }

    for (var pa = 1; pa <= 3; pa++) {
        var nomePassaro = 'passaro' + pa;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 1001 - 500);
        document.getElementById(nomePassaro).style.top = 200 + 'px';
        document.getElementById(nomePassaro).alt = 'vivo';

        if (pa === 1) {
            document.getElementById(nomePassaro).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (pa === 2) {
            document.getElementById(nomePassaro).style.left = 5000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else {
            document.getElementById(nomePassaro).style.left = 8100 + posicaoInicialAleatoriaInimigo + 'px';
        }
    }


//--------------Background 5----------------------------------------------------------------------------
    for (var croc = 1; croc <= 3; croc++) {
        var nomeCrocodilo = 'crocodilo' + croc;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 601 - 300);
        document.getElementById(nomeCrocodilo).style.top = 600 - 90 - 55.59 + 'px';
        document.getElementById(nomeCrocodilo).alt = 'vivo';

        if (croc === 1) {
            document.getElementById(nomeCrocodilo).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (croc === 2) {
            document.getElementById(nomeCrocodilo).style.left = 3500 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else {
            document.getElementById(nomeCrocodilo).style.left = 7200 + posicaoInicialAleatoriaInimigo + 'px';
        }

    }
    for (var ra = 1; ra <= 3; ra++) {
        var nomeRaccoon = 'raccoon' + ra;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 401 - 200);
        document.getElementById(nomeRaccoon).style.top = 600 - 90 - 150 + 'px';
        document.getElementById(nomeRaccoon).alt = 'vivo';

        if (ra === 1) {
            document.getElementById(nomeRaccoon).style.left = 4000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (ra === 2) {
            document.getElementById(nomeRaccoon).style.left = 6500 + posicaoInicialAleatoriaInimigo + 'px';
        }

        else {
            document.getElementById(nomeRaccoon).style.left = 9000 + posicaoInicialAleatoriaInimigo + 'px';
        }

    }

    for (var a = 1; a <= 3; a++) {
        var nomeAguia = 'aguia' + a;
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 1001 - 500);
        document.getElementById(nomeAguia).style.top = 200 + 'px';
        document.getElementById(nomeAguia).alt = 'vivo';

        if (a === 1) {
            document.getElementById(nomeAguia).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else if (a === 2) {
            document.getElementById(nomeAguia).style.left = 5000 + posicaoInicialAleatoriaInimigo + 'px';
        }
        else {
            document.getElementById(nomeAguia).style.left = 8100 + posicaoInicialAleatoriaInimigo + 'px';
        }
    }


    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //----------Sapo,etc----------------------------------

    personagens.innerHTML += '<img id="jogador" src="imagens/sapo/sapo1.png" height="106" width="112" style="position: absolute">';
    sapo = document.getElementById('jogador');
    sapo.style.left = posicaoInicial + 'px';
    sapo.style.top = 600 - 90 - 106 + 'px';
    opcoes_btn.onmouseover = function () {
        opcoes_btn.src = 'imagens/botoes/opcoesHover_btn.png';
        opcoes_btn.style.cursor = 'pointer';
    };

    opcoes_btn.onmouseout = function () {
        opcoes_btn.src = 'imagens/botoes/opcoes_btn.png';
    };

    opcoes_btn.onclick = function () {
        menuOpcoes();
    };

    jogar_btn.onmouseover = function () {
        jogar_btn.src = 'imagens/botoes/jogarHover_btn.png';
        jogar_btn.style.cursor = 'pointer';
    };

    jogar_btn.onmouseout = function () {
        jogar_btn.src = 'imagens/botoes/jogar_btn.png';
    };
    jogar_btn.onclick = function () {
        ecraInicial.style.display = 'none';
        ecraJogo.style.display = 'initial';
        corpo.setAttribute('class', 'stop-scrolling');
        //$('body').addClass('stop-scrolling');
        carregaJogo();
    };
    som_btn.onmouseover = function () {
        som_btn.style.cursor = 'pointer';
    };
    som_btn.onclick = function () {
        if (somMute === true) {

            somFundo1.play();
            somMute = false;
            som_btn.src = 'imagens/botoes/somON_btn.png';
        } else {
            somFundo1.pause();
            somMute = true;
            som_btn.src = 'imagens/botoes/somOFF_btn.png';
        }
    };
    //---------------------Modo Batalha-------------------//
    modoBatalha.style.top = '530px';
    modoBatalha.innerHTML += '<table style="vertical-align: middle"><tr><th>ATACAR</th></tr><tr><th>FUGIR</th></tr></table>';
    background();

    //------------

    changelevel = document.getElementById("changelevel");
    changelevel.innerHTML = "<h1>Nível " + (parseInt(nlvl) + 1) + "</h1>";

    //------------

    acabouSe = document.getElementById('fimJogo');
    acabouSe.innerHTML = 'Pontuação Máxima: ';
    acabouSe.style.display = 'none';
}


//----------------------------------------------------------------------

function comecaJogar() {
    //-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-..-..-.-.-.-.--.-.//
    verificaRaminho = setInterval(rodaRamosPedras, 25);
    verificaInsectchinhoLixinho = setInterval(rodaInsectosLixosBuracos, 25);
    geralTimer = setInterval(atualizaJogo, 25);
    //verifcaSeEstasVivoPa = setInterval(notDeadYet, 25);
}

function notDeadYet() {
    if (vidaAtual <= 0 && auxilioBatalha === false){
        contVidas--;
        if (contVidas === 2) {
            vidaAtual = 150;
            document.getElementById('vida3Img').style.visibility = 'hidden';
        }
        if (contVidas === 1) {
            vidaAtual = 150;
            document.getElementById('vida2Img').style.visibility = 'hidden';
        }
        if (contVidas === 0) {
            document.getElementById('vida1Img').style.visibility = 'hidden';
            fimJogo();
        }
    }


}

function atualizaJogo() {
    executaTeclas();
}

function fimJogo() {
    clearInterval(desSardao);
    clearInterval(moveSardao);
    clearInterval(desMocho);
    clearInterval(moveMocho);
    clearInterval(desOurico);
    clearInterval(moveOurico);
    clearInterval(desCobra);
    clearInterval(moveCobra);
    clearInterval(desRaccoon);
    clearInterval(moveRaccoon);
    clearInterval(desAguia);
    clearInterval(moveAguia);
    clearInterval(desCrocodilo);
    clearInterval(moveCrocodilo);
    clearInterval(desDodo);
    clearInterval(moveDodo);
    clearInterval(desSardao2);
    clearInterval(moveSardao2);
    clearInterval(desSardao3);
    clearInterval(moveSardao3);
    clearInterval(desBat);
    clearInterval(moveBat);
    clearInterval(desPassaro);
    clearInterval(movePassaro);
    //--------------------
    clearInterval(desNinjaPreto);
    clearInterval(desNinjaAzul);
    clearInterval(desNinjaRoxo);
    clearInterval(desNinjaVerde);
    clearInterval(desNinjaVermelho);
    somFundo1.muted = true;
    somSaltar.muted = true;
    somModoBatalha.muted = true;
    somPerdeVidaOuHP.muted = true;
    somGanhaVidaOuXP.muted = true;
    somMudarSeta.muted = true;

    ecraInicial.style.display = 'none';
    acabouSe.style.display = 'block';
    acabouSe.innerHTML = "<img id='logo_ecrajogo' src='imagens/logo.png' >" + "<h1>Pontuação=" + " " + pontuacao + "<h1>";

}

function fazResetParaLvlUp() {
    clearInterval(desSardao);
    clearInterval(moveSardao);
    clearInterval(desMocho);
    clearInterval(moveMocho);
    clearInterval(desOurico);
    clearInterval(moveOurico);
    clearInterval(desCobra);
    clearInterval(moveCobra);
    clearInterval(desRaccoon);
    clearInterval(moveRaccoon);
    clearInterval(desAguia);
    clearInterval(moveAguia);
    clearInterval(desCrocodilo);
    clearInterval(moveCrocodilo);
    clearInterval(desDodo);
    clearInterval(moveDodo);
    clearInterval(desSardao2);
    clearInterval(moveSardao2);
    clearInterval(desSardao3);
    clearInterval(moveSardao3);
    clearInterval(desBat);
    clearInterval(moveBat);
    clearInterval(desPassaro);
    clearInterval(movePassaro);
    //--------------------
    clearInterval(desNinjaPreto);
    clearInterval(desNinjaAzul);
    clearInterval(desNinjaRoxo);
    clearInterval(desNinjaVerde);
    clearInterval(desNinjaVermelho);
    contVidas = 3;
    for (var vidinha = 1; vidinha <= 3; vidinha++) {
        var restauraVida = 'vida' + vidinha + 'Img';
        document.getElementById(restauraVida).alt = 'yummi';
        document.getElementById(restauraVida).style.visibility = 'visible';
    }
    sapo.style.left = posicaoInicial + 'px';
    sapo.style.top = 600 - 90 - 106 + 'px';
    carregaJogo();
    background();
    if (nlvl % 2 !== 0) {
        //-----------Objetos------------------------------------------------------------------------------------------------------------------------------------

        // ----- lixo ---------------

        for (var gu = 1; gu <= 3; gu++) {
            var lixinho = 'garbage' + gu;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 400);
            document.getElementById(lixinho).style.top = 600 - 90 - 70 + 'px';

            if (gu === 1) {
                document.getElementById(lixinho).style.left = 800 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (gu === 2) {
                document.getElementById(lixinho).style.left = 4000 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(lixinho).style.left = 7800 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }
        // ----- pedras ---------------

        for (var rk = 1; rk <= 3; rk++) {
            var pedrinha = 'rock' + rk;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 200);
            document.getElementById(pedrinha).style.top = 600 - 90 - 135 + 'px';

            if (rk === 1) {
                document.getElementById(pedrinha).style.left = 1400 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else if (rk === 2) {
                document.getElementById(pedrinha).style.left = 5000 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(pedrinha).style.left = 7000 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }
        // ----- troncos ---------------

        for (var bc = 1; bc <= 5; bc++) {
            var raminho = 'branch' + bc;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 200);
            document.getElementById(raminho).style.top = 600 - 250 + 'px';

            if (bc === 1) {
                document.getElementById(raminho).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (bc === 2) {
                document.getElementById(raminho).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else if (bc === 3) {
                document.getElementById(raminho).style.left = 3700 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else if (bc === 4) {
                document.getElementById(raminho).style.left = 6400 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(raminho).style.left = 8700 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }
        //-------------.......................INSECTOS...................--------------//
        //-.-.-.-.-.-.-.-.-.--.-.-.
        for (var azulzinho = 1; azulzinho <= 10; azulzinho++) {
            var azulInsecto = 'azul' + azulzinho;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 200);

            document.getElementById(azulInsecto).style.top = 600 - 350 + 'px';
            document.getElementById(azulInsecto).alt = 'yummi';
            document.getElementById(azulInsecto).style.visibility = 'visible';

            if (azulzinho === 1) {
                document.getElementById(azulInsecto).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (azulzinho === 2) {
                document.getElementById(azulInsecto).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (azulzinho === 3) {
                document.getElementById(azulInsecto).style.left = 2500 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (azulzinho === 4) {
                document.getElementById(azulInsecto).style.left = 3100 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (azulzinho === 5) {
                document.getElementById(azulInsecto).style.left = 3800 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (azulzinho === 6) {
                document.getElementById(azulInsecto).style.left = 4900 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (azulzinho === 7) {
                document.getElementById(azulInsecto).style.left = 5500 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (azulzinho === 8) {
                document.getElementById(azulInsecto).style.left = 6100 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (azulzinho === 9) {
                document.getElementById(azulInsecto).style.left = 7850 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else {
                document.getElementById(azulInsecto).style.left = 9000 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }
        //-.-.-.-.-.-.-.-.-.--.-.-.
        for (var verdinho = 1; verdinho <= 5; verdinho++) {
            var verdeInsecto = 'verde' + verdinho;
            document.getElementById(verdeInsecto).alt = 'yummi';
            document.getElementById(verdeInsecto).style.visibility = 'visible';
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 200);
            document.getElementById(verdeInsecto).style.top = 600 - 350 + 'px';

            if (verdinho === 1) {
                document.getElementById(verdeInsecto).style.left = 1500 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (verdinho === 2) {
                document.getElementById(verdeInsecto).style.left = 3500 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (verdinho === 3) {
                document.getElementById(verdeInsecto).style.left = 4100 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (verdinho === 4) {
                document.getElementById(verdeInsecto).style.left = 6800 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else {
                document.getElementById(verdeInsecto).style.left = 8350 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }
        //-.-.-.-.-.-.-.-.-.--.-.-.
        posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 7700);
        document.getElementById('dourado').alt = 'yummi';
        document.getElementById('dourado').style.visibility = 'visible';
        document.getElementById('dourado').style.top = 600 - 350 + 'px';
        document.getElementById('dourado').style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
        //----------------------buraco---------------------------------
        for (var hb = 1; hb <= 2; hb++) {
            var buracozinho = 'hole' + hb;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 401 - 200);
            document.getElementById(buracozinho).alt = 'yummi';
            document.getElementById(buracozinho).style.visibility = 'visible';
            document.getElementById(buracozinho).style.top = 600 - 100 + "px";

            if (hb === 1) {
                document.getElementById(buracozinho).style.left = 2850 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else {
                document.getElementById(buracozinho).style.left = 8550 + posicaoInicialAleatoriaInimigo + 'px';
            }

        }
    }
    if (nlvl === 2) {
        somFundo1.src = 'sons/fundo/lv2.mp3';
        somFundo1.play();
        document.getElementById('ninja_verde').style.top = 600 - 90 - 300 + 'px';
        document.getElementById('ninja_verde').style.left = 350 + 'px';
        document.getElementById('ninja_verde').alt = 'yummi';
        document.getElementById('ninja_verde').style.visibility = 'visible';
    }
    if (nlvl === 3) {
        somFundo1.src = 'sons/fundo/lv3.mp3';
        somFundo1.play();
        for (var sard = 1; sard <= 3; sard++) {
            var SardaoNome = 'sardao' + sard;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 601 - 300);
            document.getElementById(SardaoNome).style.top = 600 - 90 - 61 + 'px';
            document.getElementById(SardaoNome).alt = 'vivo';
            document.getElementById(SardaoNome).style.visibility = 'visible';

            if (sard === 1) {
                document.getElementById(SardaoNome).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (sard === 2) {
                document.getElementById(SardaoNome).style.left = 3500 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(SardaoNome).style.left = 7200 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }

        for (var yu = 1; yu <= 3; yu++) {
            var Mochonome = 'mocho' + yu;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 1001 - 500);
            document.getElementById(Mochonome).style.top = 102 + 'px';
            document.getElementById(Mochonome).alt = 'vivo';
            document.getElementById(Mochonome).style.visibility = 'visible';

            if (yu === 1) {
                document.getElementById(Mochonome).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (yu === 2) {
                document.getElementById(Mochonome).style.left = 5000 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(Mochonome).style.left = 8100 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }

        for (var ou = 1; ou <= 3; ou++) {
            var OuricoNome = 'ourico' + ou;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 401 - 200);
            document.getElementById(OuricoNome).style.top = 600 - 90 - 87 + 'px';
            document.getElementById(OuricoNome).alt = 'vivo';
            document.getElementById(OuricoNome).style.visibility = 'visible';


            if (ou === 1) {
                document.getElementById(OuricoNome).style.left = 4000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (ou === 2) {
                document.getElementById(OuricoNome).style.left = 6500 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(OuricoNome).style.left = 9000 + posicaoInicialAleatoriaInimigo + 'px';
            }

        }
    }
    if (nlvl === 4) {
        somFundo1.src = 'sons/fundo/lv4.mp3';
        somFundo1.play();
        document.getElementById('ninja_azul').style.top = 600 - 90 - 300 + 'px';
        document.getElementById('ninja_azul').style.left = 350 + 'px';
        document.getElementById('ninja_azul').alt = 'yummi';
        document.getElementById('ninja_azul').style.visibility = 'visible';
    }
    if (nlvl === 5) {
        somFundo1.src = 'sons/fundo/lv5.mp3';
        somFundo1.play();
        for (var sard2 = 1; sard2 <= 3; sard2++) {
            var Sardao2Nome = 'sardao2_' + sard2;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 601 - 300);
            document.getElementById(Sardao2Nome).style.top = 600 - 90 - 61 + 'px';
            document.getElementById(Sardao2Nome).alt = 'vivo';
            document.getElementById(Sardao2Nome).style.visibility = 'visible';

            if (sard2 === 1) {
                document.getElementById(Sardao2Nome).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (sard2 === 2) {
                document.getElementById(Sardao2Nome).style.left = 3500 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(Sardao2Nome).style.left = 7200 + posicaoInicialAleatoriaInimigo + 'px';
            }

        }
        for (var cob = 1; cob <= 3; cob++) {
            var CobraNome = 'cobra' + cob;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 401 - 200);
            document.getElementById(CobraNome).style.top = 600 - 90 - 129 + 'px';
            document.getElementById(CobraNome).alt = 'vivo';
            document.getElementById(CobraNome).style.visibility = 'visible';

            if (cob === 1) {
                document.getElementById(CobraNome).style.left = 4000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (cob === 2) {
                document.getElementById(CobraNome).style.left = 6500 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(CobraNome).style.left = 9000 + posicaoInicialAleatoriaInimigo + 'px';
            }

        }
        for (var batT = 1; batT <= 3; batT++) {
            var BatNome = 'bat' + batT;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 1001 - 500);
            document.getElementById(BatNome).style.top = 102 + 'px';
            document.getElementById(BatNome).alt = 'vivo';
            document.getElementById(BatNome).style.visibility = 'visible';
            if (batT === 1) {
                document.getElementById(BatNome).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (batT === 2) {
                document.getElementById(BatNome).style.left = 5000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else {
                document.getElementById(BatNome).style.left = 8100 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }
    }
    if (nlvl === 6) {
        somFundo1.src = 'sons/fundo/lv6.mp3';
        somFundo1.play();
        document.getElementById('ninja_roxo').style.top = 600 - 90 - 300 + 'px';
        document.getElementById('ninja_roxo').style.left = 350 + 'px';
        document.getElementById('ninja_roxo').alt = 'yummi';
        document.getElementById('ninja_roxo').style.visibility = 'visible';
    }
    if (nlvl === 7) {
        somFundo1.src = 'sons/fundo/lv7.mp3';
        somFundo1.play();
        for (var dod = 1; dod <= 3; dod++) {
            var DodoNome = 'dodo' + dod;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 601 - 300);
            document.getElementById(DodoNome).style.top = 600 - 90 - 105 + 'px';
            document.getElementById(DodoNome).alt = 'vivo';
            document.getElementById(DodoNome).style.visibility = 'visible';
            if (dod === 1) {
                document.getElementById(DodoNome).style.left = 4000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (dod === 2) {
                document.getElementById(DodoNome).style.left = 6500 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(DodoNome).style.left = 9000 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }

        for (var sdr3 = 1; sdr3 <= 3; sdr3++) {
            var Sardao3Nome = 'sardao3_' + sdr3;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 601 - 300);
            document.getElementById(Sardao3Nome).style.top = 600 - 90 - 61 + 'px';
            document.getElementById(Sardao3Nome).alt = 'vivo';
            document.getElementById(Sardao3Nome).style.visibility = 'visible';

            if (sdr3 === 1) {
                document.getElementById(Sardao3Nome).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (sdr3 === 2) {
                document.getElementById(Sardao3Nome).style.left = 3500 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else {
                document.getElementById(Sardao3Nome).style.left = 7200 + posicaoInicialAleatoriaInimigo + 'px';
            }

        }

        for (var pat = 1; pat <= 3; pat++) {
            var PassaroNome = 'passaro' + pat;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 1001 - 500);
            document.getElementById(PassaroNome).style.top = 200 + 'px';
            document.getElementById(PassaroNome).alt = 'vivo';
            document.getElementById(PassaroNome).style.visibility = 'visible';

            if (pat === 1) {
                document.getElementById(PassaroNome).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (pat === 2) {
                document.getElementById(PassaroNome).style.left = 5000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else {
                document.getElementById(PassaroNome).style.left = 8100 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }


    }
    if (nlvl === 8) {
        somFundo1.src = 'sons/fundo/lv6.mp3';
        somFundo1.play();
        document.getElementById('ninja_vermelho').style.top = 600 - 90 - 300 + 'px';
        document.getElementById('ninja_vermelho').style.left = 350 + 'px';
        document.getElementById('ninja_vermelho').alt = 'yummi';
        document.getElementById('ninja_vermelho').style.visibility = 'visible';
    }
    if (nlvl === 9) {
        somFundo1.src = 'sons/fundo/lv7.mp3';
        somFundo1.play();
        for (var crod = 1; crod <= 3; crod++) {
            var CrocodiloNome = 'crocodilo' + crod;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 601 - 300);
            document.getElementById(CrocodiloNome).style.top = 600 - 90 - 55.59 + 'px';
            document.getElementById(CrocodiloNome).alt = 'vivo';
            document.getElementById(CrocodiloNome).style.visibility = 'visible';

            if (crod === 1) {
                document.getElementById(CrocodiloNome).style.left = 1000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (crod === 2) {
                document.getElementById(CrocodiloNome).style.left = 3500 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(CrocodiloNome).style.left = 7200 + posicaoInicialAleatoriaInimigo + 'px';
            }

        }

        for (var rac = 1; rac <= 3; rac++) {
            var RaccoonNome = 'raccoon' + rac;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 401 - 200);
            document.getElementById(RaccoonNome).style.top = 600 - 90 - 150 + 'px';
            document.getElementById(RaccoonNome).alt = 'vivo';
            document.getElementById(RaccoonNome).style.visibility = 'visible';

            if (rac === 1) {
                document.getElementById(RaccoonNome).style.left = 4000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (rac === 2) {
                document.getElementById(RaccoonNome).style.left = 6500 + posicaoInicialAleatoriaInimigo + 'px';
            }

            else {
                document.getElementById(RaccoonNome).style.left = 9000 + posicaoInicialAleatoriaInimigo + 'px';
            }

        }

        for (var guia = 1; guia <= 3; guia++) {
            var AguiaNome = 'aguia' + guia;
            posicaoInicialAleatoriaInimigo = parseInt(Math.random() * 1001 - 500);
            document.getElementById(AguiaNome).style.top = 200 + 'px';
            document.getElementById(AguiaNome).alt = 'vivo';
            document.getElementById(AguiaNome).style.visibility = 'visible';
            if (guia === 1) {
                document.getElementById(AguiaNome).style.left = 2000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else if (guia === 2) {
                document.getElementById(AguiaNome).style.left = 5000 + posicaoInicialAleatoriaInimigo + 'px';
            }
            else {
                document.getElementById(AguiaNome).style.left = 8100 + posicaoInicialAleatoriaInimigo + 'px';
            }
        }
    }
    if (nlvl === 10) {
        somFundo1.src = 'sons/fundo/lv7.mp3';
        somFundo1.play();
        document.getElementById('ninja_preto').style.top = 600 - 90 - 300 + 'px';
        document.getElementById('ninja_preto').style.left = 350 + 'px';
        document.getElementById('ninja_preto').alt = 'yummi';
        document.getElementById('ninja_preto').style.visibility = 'visible';
    }
    if (nlvl === 11) {
        fimJogo();
    }
    //------------pedra dos 10000 px -----------------------
    document.getElementById('endrock').alt = 'yummi';
    document.getElementById('endrock').style.visibility = 'visible';
    document.getElementById('endrock').style.top = 600 - 90 - 251 + 'px';
    document.getElementById('endrock').style.left = 10000 + 'px';
    //-.-.-.-.-.-.-.
    //.-.-.-.-.-.-.-.

}