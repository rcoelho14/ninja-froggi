No âmbito da disciplina de Laboratório Multimédia
III foi-nos lançado o desafio de fazer um jogo
utilizando os conteúdos aprendidos ao longo do
semestre recorrendo também a alguns temas
lecionados anteriormente.

A associação BioLiving foi o tema escolhido pelos
professores para os projetos. Esta associação
enquadra conteúdos temáticos relacionados com a
natureza e a proteção do ambiente dos animais.
Desta forma, teríamos que desenvolver um jogo
relacionado com estes temas recorrendo a HTML,
CSS e JavaScript.

Após a divulgação deste tema e tendo em conta
todas as exigências do projecto decidimos, em
grupo, fazer um jogo que envolvesse o sapo e a
perda do seu habitat devido às alterações climáticas
e à poluição, que de certa forma envolvem a mão do
Homem.

Neste jogo tentámos que fosse possivel englobar
todos os conhecimentos adquiridos aos longo das
aulas e aqueles que fomos obtendo através de fontes
exteriores.